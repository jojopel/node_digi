var PrettyError = require('pretty-error')
var pe = new PrettyError()
pe.skipNodeFiles()
pe.skipPackage('express')
module.exports = pe
