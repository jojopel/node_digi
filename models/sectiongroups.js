module.exports = function (sequelize, DataTypes) {
  return sequelize.define('sectiongroups', {
    sectiongroup_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    sectiongroup_name: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    timestamps: false
  })
}
