const fs = require('fs')
const path = require('path')
const Sequelize = require('sequelize')
const {logger} = require('../loaders/logger')

const config = require('../config')

const basename = path.basename(__filename)
const env = process.env.NODE_ENV ? process.env.NODE_ENV : 'development'

const db = {}

let sequelize = new Sequelize(config[env].dbname, config[env].dbuser, config[env].dbuser, {
  host: config[env].host,
  port: config[env].dbport,
  dialect: config[env].dialect,
  logging: msg => {
    logger.mylogger.SequelizeLogger.info(msg)
  },
  // logging: true,
  pool: {
    max: 20,
    min: 0,
    idle: 10000
  }
})

fs.readdirSync(__dirname)
  .filter((file) => {
    return file.indexOf('.') !== 0 && file !== basename && file.slice(-3) === '.js' && (file !== 'init.js')
  })
  .forEach((file) => {
    const model = sequelize.import(path.join(__dirname, file))
    db[model.name] = model
  })

Object.keys(db).forEach((modelName) => {
  if (db[modelName].associate) {
    db[modelName].associate(db)
  }
})

db.sequelize = sequelize
db.Sequelize = Sequelize

module.exports = db
