module.exports = function (sequelize, DataTypes) {
  const PageHistory = sequelize.define('pageshistory',
    {
      pagehistory_id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
      },
      page_id: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      page_json: {
        type: DataTypes.TEXT,
        defaultValue: '[]',
        get: function () {
          return this.getDataValue('page_json')
        },
        set: function (value) {
          this.setDataValue('page_json', JSON.stringify(value))
        }
      },
      createdAt: {
        field: 'created',
        type: DataTypes.DATE,
        allowNull: true
      },
      updatedAt: {
        field: 'modified',
        type: DataTypes.DATE,
        allowNull: true
      },
      created_by: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      modified_by: {
        type: DataTypes.INTEGER,
        allowNull: false
      }
    },
    {
      freezeTableName: true,
      tableName: 'pageshistory'
    })

  PageHistory.associate = (models) => {
    PageHistory.belongsTo(models.users, {
      foreignKey: 'created_by',
      sourceKey: 'user_id'
    })
  }

  return PageHistory
}
