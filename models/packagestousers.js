module.exports = function (sequelize, DataTypes) {
  const PackagesToUsers = sequelize.define('packagestousers', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    package_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  },
  {
    hooks: {
      beforeCreate: (assigned, options) => {
        return sequelize.query('SELECT exportflag FROM packages WHERE id = ?', {
          replacements: [assigned.package_id],
          type: sequelize.QueryTypes.SELECT
        }).then((data) => {
          if (data[0].exportflag === 1) {
            throw new Error("I'm afraid I can't let you do that! Package Export Process Underway!")
          }
        }).catch((error) => {
          throw new Error(error)
        })
      },
      beforeDestroy: (assigned, options) => {
        console.log('destroy')
        return sequelize.query('SELECT exportflag FROM packages WHERE id = ?', {
          replacements: [assigned.package_id],
          type: sequelize.QueryTypes.SELECT
        }).then((data) => {
          if (data[0].exportflag === 1) {
            throw new Error("I'm afraid I can't let you do that! Package Export Process Underway!")
          }
        }).catch((error) => {
          throw new Error(error)
        })
      },
      beforeUpdate: (assigned, options) => {
        return sequelize.Promise.try(() => {
          return sequelize.query('SELECT exportflag FROM packages WHERE id = ?', {
            replacements: [assigned.package_id],
            type: sequelize.QueryTypes.SELECT
          }).then((data) => {
            if (data[0].exportflag === 1) {
              throw new Error("I'm afraid I can't let you do that! Package Export Process Underway!")
            }
          }).catch((error) => {
            throw new Error(error)
          })
        })
      }
    },
    timestamps: false
  })

  PackagesToUsers.associate = (models) => {
  }

  return PackagesToUsers
}
