module.exports = function (sequelize, DataTypes) {
  const Section = sequelize.define('sections', {
    section_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    section_name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        isUniqueSectionName (value, next) {
          sequelize.query('SELECT count(1) as counter FROM sections WHERE section_name = ? ', {
            replacements: [value],
            type: sequelize.QueryTypes.SELECT
          }).then(users => {
            if (users[0].counter > 0) {
              next('Section Name is already in use')
            } else {
              next()
            }
          })
        }
      }
    },
    section_category: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    section_area: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    section_group: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    section_json: {
      type: DataTypes.TEXT,
      defaultValue: '[]',
      get: function () {
        return this.getDataValue('section_json')
      },
      set: function (value) {
        value[0].name = this.getDataValue('section_name')
        this.setDataValue('section_json', JSON.stringify(value))
      }
    },
    createdAt: {
      field: 'created',
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedAt: {
      field: 'modified',
      type: DataTypes.DATE,
      allowNull: true
    },
    created_by: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    modified_by: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  })

  Section.associate = (models) => {
    Section.belongsTo(models.packagecategories, {
      foreignKey: 'section_category',
      sourceKey: 'category_id'
    })
    // sections to areas
    Section.belongsTo(models.sectionareas, {
      foreignKey: 'section_area',
      sourceKey: 'area_id'
    })
    // sections to groups
    Section.belongsTo(models.sectiongroups, {
      foreignKey: 'section_group',
      sourceKey: 'group_id'
    })
    // sections to packages through sectionstopackages model
    Section.belongsToMany(models.packages, {
      through: {
        model: models.sectionstopackages,
        unique: false
      },
      as: 'sectionpackage',
      foreignKey: 'section_id',
      constraints: false
    })
    Section.belongsTo(models.users, {
      as: 'sectionmodified',
      foreignKey: 'modified_by',
      sourceKey: 'user_id'
    })
    Section.belongsTo(models.users, {
      as: 'creator_role',
      foreignKey: 'created_by',
      sourceKey: 'user_id'
    })
  }

  return Section
}
