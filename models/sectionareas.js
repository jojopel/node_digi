module.exports = function (sequelize, DataTypes) {
  return sequelize.define('sectionareas', {
    area_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    area_name: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    timestamps: false
  })
}
