
// var servicesContainer = require('../services/')
var pe = require('../helpers/errorConfig')
var path = require('path')
const db = require('../models')
var Mylogger = require('../server/main')

var packagefolder = null

const findpackage = (id) => {
  return db.packages.findOne({where: {id: id}})
}

const viewPageController = {
  viewPackageById (req, res) {
    findpackage(req.params.id).then((response) => {
      packagefolder = response.package_folder
      res.render(response.package_folder + '/dist/index.html')
    }).catch((error) => {
      console.log(pe.render(new Error('Iframe View Package')))
      console.log(pe.render(error))
      Mylogger.mylogger.SequelizeLogger.error(error.message)
      return res.status(500).json({
        errorflag: true,
        message: error
      })
    })
  },
  getCssFile (req, res) {
    var cssfile = path.join(__dirname, '../../', '/projects_dist/' + packagefolder + '/dist/static/css/' + req.params.file)
    res.sendFile(cssfile)
  },
  getJsFile (req, res) {
    var jsfile = path.join(__dirname, '../../', '/projects_dist/' + packagefolder + '/dist/static/js/' + req.params.file)
    res.sendFile(jsfile)
  },
  getImgFile (req, res) {
    var imgfile = path.join(__dirname, '../../', '/projects_dist/' + packagefolder + '/dist/static/img/' + req.params.file)
    res.sendFile(imgfile)
  },
  getMediaFile (req, res) {
    var mediafile = path.join(__dirname, '../../', '/projects_dist/' + packagefolder + '/dist/static/media/' + req.params.file)
    res.sendFile(mediafile)
  }
}

module.exports = viewPageController
