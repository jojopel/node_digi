const serviceContainer = require('../services')
const db = require('../models')
// var Mylogger = require('../server/main')
var pe = require('../helpers/errorConfig')

const userController = {
  getUser (req, res) {
    serviceContainer.userService
      .getUser(req.user.user_id)
      .then((user) => {
        if (user) {
          return res.status(200).json({
            error: false,
            message: 'Found the user',
            user: user
          })
        }
        return res.status(400).json({
          error: true,
          message: 'No such user id'
        })
      })
      .catch((error) => {
        return res.status(500).json({
          error: true,
          message: error
        })
      })
  },
  getCurrentUserProfile (req, res) {
    db.users.findOne({
      where: {
        user_id: req.user.user_id
      }
    }).then((user) => {
      if (!user) {
        return res.status(201).json({
          errorflag: true,
          message: 'User not found'
        })
      } else if (user) {
        return res.status(200).json({
          errorflag: false,
          message: 'User found',
          user: user
        })
      }
    }).catch((err) => {
      console.log(pe.render(new Error('Find One User')))
      console.log(pe.render(err))
      // Mylogger.mylogger.SequelizeLogger.error(err.message)
      return res.status(500).json({
        errorflag: true,
        message: err
      })
    })
  },
  getUserProfile (req, res) {
    db.users.findOne({
      where: {
        user_id: req.params.id
      }
    }).then((user) => {
      if (!user) {
        return res.status(201).json({
          errorflag: true,
          message: 'User not found'
        })
      } else if (user) {
        return res.status(200).json({
          errorflag: false,
          message: 'User found',
          user: user
        })
      }
    }).catch((err) => {
      console.log(pe.render(new Error('Find One User')))
      console.log(pe.render(err))
      // Mylogger.mylogger.SequelizeLogger.error(err.message)
      return res.status(500).json({
        errorflag: true,
        message: err
      })
    })
  },
  createUser (req, res) {
    var user = db.users.build(req.body.data)
    user.created_by = req.user.user_id
    user.modified_by = req.user.user_id
    user.validate().then(() => {
      // var hashpassword = require('crypto').createHash('sha256').update(user.dataValues.password).digest('base64')
      // user.dataValues.password = hashpassword
      user.save().then(() => {
        return res.status(200).json({
          errorflag: false,
          message: 'User Saved Succesfully'
        })
      }).catch((err) => {
        console.log(pe.render(new Error('Save User')))
        console.log(pe.render(err))
        // Mylogger.mylogger.SequelizeLogger.error(err.message)
        return res.status(500).json({
          errorflag: true,
          message: err
        })
      })
    }).catch((err) => {
      console.log(pe.render(err))
      // Mylogger.mylogger.SequelizeLogger.error(err.message)
      return res.status(201).json({
        errorflag: true,
        message: err
      })
    })
  },
  updateUser (req, res) {
    if (req.user.user_id !== req.body.data.user_id) {
      console.log('update other')
      db.users.findOne({
        where: {
          user_id: req.body.data.user_id
        }
      }).then(user => {
        if (!user) {
          return res.status(201).json({
            errorflag: true,
            message: 'User not found'
          })
        } else {
          if (req.body.data.password !== null) {
            console.log('with pwd')
            // var hashpassword = require('crypto').createHash('sha256').update(req.body.data.password).digest('base64')
            // req.body.data.password = hashpassword
            user.update({
              firstname: req.body.data.firstname,
              lastname: req.body.data.lastname,
              username: req.body.data.username,
              password: req.body.data.password,
              email: req.body.data.email,
              role_id: req.body.data.role_id,
              user_status: req.body.data.user_status,
              modified_by: req.user.user_id
            }).then((response) => {
              return res.status(200).json({
                errorflag: false,
                message: 'User Updated Succesfully'
              })
            }).catch((err) => {
              console.log(pe.render(err))
              // Mylogger.mylogger.SequelizeLogger.error(err.message)
              return res.status(201).json({
                errorflag: true,
                message: err
              })
            })
          } else {
            console.log('no pwd')
            // req.body.data.password = user.password
            user.update({
              firstname: req.body.data.firstname,
              lastname: req.body.data.lastname,
              username: req.body.data.username,
              email: req.body.data.email,
              role_id: req.body.data.role_id,
              user_status: req.body.data.user_status,
              modified_by: req.user.user_id
            }).then((response) => {
              return res.status(200).json({
                errorflag: false,
                message: 'User Updated Succesfully'
              })
            }).catch((err) => {
              console.log(pe.render(err))
              // Mylogger.mylogger.SequelizeLogger.error(err.message)
              return res.status(201).json({
                errorflag: true,
                message: err
              })
            })
          }
        }
      }).catch((err) => {
        console.log(pe.render(new Error('Find One User')))
        console.log(pe.render(err))
        // Mylogger.mylogger.SequelizeLogger.error(err.message)
        return res.status(500).json({
          errorflag: true,
          message: err
        })
      })
    } else if (req.user.user_id === req.body.data.user_id) {
      console.log('update self')
      if (req.body.data.password !== null) {
        console.log('with pwd')
        db.users.findOne({
          where: {
            user_id: req.user.user_id
          }
        }).then(user => {
          user.update({
            firstname: req.body.data.firstname,
            lastname: req.body.data.lastname,
            username: req.body.data.username,
            password: req.body.data.password,
            email: req.body.data.email,
            role_id: req.body.data.role_id,
            user_status: req.body.data.user_status,
            modified_by: req.user.user_id
          }).then((response) => {
            return res.status(200).json({
              errorflag: false,
              message: 'User Updated Successfully / Please re login!',
              self: true,
              pwd: true
            })
          }).catch((err) => {
            console.log(pe.render(err))
            // Mylogger.mylogger.SequelizeLogger.error(err.message)
            return res.status(201).json({
              errorflag: true,
              message: err
            })
          })
        })
        // var hashpassword = require('crypto').createHash('sha256').update(req.body.data.password).digest('base64')
        // req.body.data.password = hashpassword
      } else {
        console.log('no pwd')
        db.users.findOne({
          where: {
            user_id: req.user.user_id
          }
        }).then(user => {
        // req.body.data.password = user.password
          user.update({
            firstname: req.body.data.firstname,
            lastname: req.body.data.lastname,
            username: req.body.data.username,
            email: req.body.data.email,
            role_id: req.body.data.role_id,
            user_status: req.body.data.user_status,
            modified_by: req.user.user_id
          }).then((response) => {
            return res.status(200).json({
              errorflag: false,
              message: 'Your Info Updated Successfully!',
              self: true,
              pwd: false
            })
          }).catch((err) => {
            console.log(pe.render(err))
            // Mylogger.mylogger.SequelizeLogger.error(err.message)
            return res.status(201).json({
              errorflag: true,
              message: err
            })
          })
        })
      }
    }
  },
  getUserByUsername (req, res) {
    db.users.findAll({
      include: [{
        model: db.roles,
        attributes: ['role_name']
      },
      {
        model: db.sessions
      }
      ],
      where: {
        username: {
          $like: '%' + req.params.username + '%'
        }
      }
    }).then(user => {
      if (user.length < 1) {
        return res.status(201).json({
          errorflag: true,
          message: 'Users not found'
        })
      } else {
        return res.status(200).json({
          errorflag: false,
          user: user
        })
      }
    }).catch(err => {
      console.log(pe.render(new Error('Find All Users')))
      console.log(pe.render(err))
      // Mylogger.mylogger.SequelizeLogger.error(err.message)
      return res.status(500).json({
        errorflag: true,
        message: err
      })
    })
  },
  userAndPackages (req, res) {
    db.users.findAll({
      include: [{
        model: db.packages,
        as: 'packageuser',
        attributes: ['id']
      }],
      attributes: ['user_id', 'username']
    }).then(users => {
      if (users.length < 1) {
        return res.status(201).json({
          errorflag: true,
          message: 'Users not found'
        })
      } else {
        return res.status(200).json({
          errorflag: false,
          users: users
        })
      }
    }).catch(err => {
      console.log(pe.render(new Error('Find All Users')))
      console.log(pe.render(err))
      // Mylogger.mylogger.SequelizeLogger.error(err.message)
      return res.status(500).json({
        errorflag: true,
        message: err
      })
    })
  },
  gridView (req, res) {
    if (req.body.firstname === undefined) {
      req.body.firstname = '%'
    }
    if (req.body.lastname === undefined) {
      req.body.lastname = '%'
    }
    if (req.body.email === undefined) {
      req.body.email = '%'
    }
    if (req.body.role === undefined) {
      req.body.role = '^[0-9]+'
    } else {
      req.body.role = '^[' + req.body.role + ']+'
    }
    if (req.body.status === undefined) {
      req.body.status = '^[0-9]+'
    } else {
      req.body.status = '^[' + req.body.status + ']+'
    }
    if (req.body.username === undefined) {
      req.body.username = '%'
    }
    // Query to retrieve users based on search attributes and the count of them for pagination
    db.users.findAndCountAll({
      distinct: true,
      include: [{
        model: db.roles,
        attributes: ['role_name'],
        required: true
      }],
      where: {
        $and: [{
          firstname: {
            $like: '%' + req.body.firstname + '%'
          }
        }, {
          lastname: {
            $like: '%' + req.body.lastname + '%'
          }
        }, {
          username: {
            $like: '%' + req.body.username + '%'
          }
        }, {
          email: {
            $like: '%' + req.body.email + '%'
          }
        }, {
          role_id: {
            $regexp: req.body.role
          }
        }, {
          user_status: {
            $regexp: req.body.status
          }
        }]
      },
      limit: req.body.paginationlimit,
      offset: req.body.paginationoffset
    }).then((users, count) => {
      if (users.length < 1) {
        return res.status(201).json({
          errorflag: true,
          message: 'Users not found'
        })
      } else {
        return res.status(200).json({
          errorflag: false,
          users: users
        })
      }
    }).catch(err => {
      console.log(pe.render(new Error('Find And Count All Users')))
      console.log(pe.render(err))
      // Mylogger.mylogger.SequelizeLogger.error(err.message)
      return res.status(500).json({
        errorflag: true,
        message: err
      })
    })
  },
  getRoles (req, res) {
    db.roles.findAll().then(roles => {
      return res.status(200).json({
        errorflag: false,
        roles: roles
      })
    }).catch(err => {
      console.log(pe.render(new Error('Find All Roles')))
      console.log(pe.render(err))
      // Mylogger.mylogger.SequelizeLogger.error(err.message)
      return res.status(500).json({
        errorflag: true,
        message: err
      })
    })
  },
  banUser (req, res) {
    db.users.update({
      modified_by: req.user.user_id,
      user_status: 0,
      modified: null
    }, {
      where: {
        user_id: req.body.userid
      }
    }).then(flag => {
      if (!flag) {
        return res.status(201).json({
          errorflag: true,
          message: 'Users not found'
        })
      } else {
        return res.status(200).json({
          errorflag: false,
          message: 'User Banned Successfully'
        })
      }
    }).catch(err => {
      console.log(pe.render(new Error('Update User')))
      console.log(pe.render(err))
      // Mylogger.mylogger.SequelizeLogger.error(err.message)
      return res.status(500).json({
        errorflag: true,
        message: err
      })
    })
  }
}
module.exports = userController
