
var pe = require('../helpers/errorConfig')
var fs = require('fs-extra')
var auth = require('../controllers/auth.js')
var pilotcopy = require('../controllers/pilotcopy')
const middlewares = require('../middlewares')
// var sequelize = require('../server/main.js')
const db = require('../models')
var path = require('path')
var zipit = require('../controllers/zip')
var rimraf = require('rimraf')
var Mylogger = require('../server/main.js')
// var io = require('../server/main.js').io
var {io} = require('../loaders/socket')
var exportsLogger = require('../helpers/export_logger_config')
var exec = require('child_process').exec,
  child

var router = require('express').Router()

router.post('/create', auth.required, middlewares.isAdmin, function (req, res, next) {
  if (req.err) {
    console.log(pe.render(new Error('Request Error')))
    console.log(pe.render(req.err))
    Mylogger.mylogger.SequelizeLogger.error(req.err.message)
    res.status(500)
    res.json({
      errorflag: true,
      message: req.err
    })
    res.end()
  } else if (req.user) {
    if (req.body.data) {
      var newpilot = db.pilotversions.build(req.body.data)
      newpilot.created_by = req.user.user_id
      newpilot.modified_by = req.user.user_id
      newpilot.validate().then(() => {
        newpilot.save().then(() => {
          res.status(200)
          res.json({
            errorflag: false,
            message: 'Pilot Saved Succesfully',
            pilot: newpilot
          })
          res.end()
        })
      }).catch((err) => {
        Mylogger.mylogger.SequelizeLogger.error(err.message)
        console.log(pe.render(err))
        if (err.name === 'SequelizeValidationError') {
          res.status(201)
          res.json({
            errorflag: true,
            message: err
          })
          res.end()
        } else {
          res.status(500)
          res.json({
            errorflag: true,
            message: err
          })
          res.end()
        }
      })
    } else {
      res.status(200)
      res.json({
        errorflag: false,
        message: 'You are authenticated'
      })
      res.end()
    }
  }
})

function getifFlaged (packageid) {
  return db.packages.findOne({
    where: {
      id: packageid
    }
  })
}

function getFlaged (packageid) {
  return db.packages.findOne({
    where: {
      id: packageid
    }
  })
}

function getPilot (pilotid) {
  return db.pilotversions.findOne({
    where: {
      pilot_id: pilotid
    }
  })
}
function rimrafPromise (file) {
  return new Promise((resolve, reject) => {
    rimraf(file, (err) => {
      if (err) {
        next(true, err)
        reject(err)
      }
      resolve()
    })
  })
}
function writeFilePromise (file, data) {
  return new Promise((resolve, reject) => {
    fs.writeFile(file, JSON.stringify(data, null, 4), (err) => {
      if (err) {
        next(true, err)
        reject(err)
      }
      resolve()
    })
  })
}
function writeMultipleJsonPagesPromises (pages, folder) {
  let PromisesArray = []
  pages.forEach(function (item, key) {
    tempname = item.page_name.replace(/\s/g, '-')
    pname = item.module_id + '-' + tempname
    pfile = item.page_json
    PromisesArray.push(writeFilePromise('../projects_dist/' + folder + '/src/assets/json/' + pname + '.json', pfile))
  })
  return Promise.all(PromisesArray)
}
function childProcessCommand (command, socket) {
  let commandOutput = []
  let cmd = exec(command)
  cmd.stdout.on('data', (data) => {
    commandOutput.push({data: data.split(' ').toString()})
    try {
      socket.emit('message', {error: false, message: data.split(' ').toString()})
    } catch (error) {
      exportsLogger.info('web socket build send data' + error)
      throw new Error(error)
    }
  })
  cmd.stderr.on('data', (data) => {
    try {
      socket.emit('error', {error: true, message: data})
    } catch (error) {
      exportsLogger.info('web socket build  error' + error)
      throw new Error(error)
    }
    console.log(`stderr: ${data}`)
  })
  cmd.on('close', (code) => {
    console.log(`child process exited with code ${code}`)
    try {
      socket.emit('finish', {error: false, message: `build finished child process exited with code ${code}`})
    } catch (error) {
      exportsLogger.info('web socket build finished' + error)
      throw new Error(error)
    }
  })
}
// update package db record with the last export id
function updateLastExport (packageId, pilotId, flag, error = false) {
  return new Promise((resolve, reject) => {
    db.packages.findOne({where: {id: packageId}}).then((packageData) => {
      console.log(packageData)
      // flag determing if package is in export process(1) or not (0)
      // if error = true the return package to export flag 0
      return flag ? packageData.update({exportflag: flag}) : error ? packageData.update({exportflag: 0}) : packageData.update({exportflag: flag, lastexport: pilotId})
    }).then((data) => {
      resolve()
    }).catch((error) => {
      reject(error)
    })
  })
}
function createTocPromise (pages) {
  return new Promise((resolve, reject) => {
    let toc = []
    pages.forEach(function (item, key) {
      tempname = item.page_name.replace(/\s/g, '-')
      pname = item.module_id + '-' + tempname
      pfile = item.page_json
      toc.push({
        url: pname,
        order: key,
        moduleid: item.module_id,
        modulename: item.module_name,
        module_order: item.module_order,
        pagedata: pname + '.json',
        pageorder: item.page_order,
        digi_activity_id: item.digi_activity_id,
        digi_activity_name: item.digi_activity_name,
        digi_module_id: item.digi_module_id,
        digi_module_name: item.digi_module_name,
        digi_skilltype_id: item.digi_skilltype_id,
        digi_keywords: item.digi_keywords,
        digi_category: item.digi_category,
        digi_weight: item.digi_weight,
        digi_eleclimitation: item.digi_eleclimitation,
        digi_skilltype_name: item.digi_skilltype_name
      })
    })
    if (toc.length < 1) {
      reject('Toc is Empty')
    }
    resolve(toc)
  })
}
function isDirectoryExists (directory) {
  try {
    fs.statSync(directory)
    return true
  } catch (e) {
    return false
  }
}
router.get('/allpilots', auth.required, middlewares.isAdmin, function (req, res, next) {
  exportsLogger.info('STARTED export of package #' + req.query.package)
  if (req.err) {
    console.log(pe.render(new Error('Request Error')))
    console.log(pe.render(req.err))
    Mylogger.mylogger.SequelizeLogger.error(req.err.message)
    res.status(500)
    res.json({
      errorflag: true,
      message: req.err
    })
    res.end()
    exportsLogger.info('/allpilots fail' + req.err)
  } else if (req.user) {
    db.pilotversions.findAll({
      raw: true
    }).then(pilots => {
      let p = pilots.map(pilots => new Object({
        pid: pilots.pilot_id,
        pilotname: pilots.pilot_name
      }))
      res.json({
        errorflag: false,
        pilot: p
      })
      res.end()
      exportsLogger.info('/allpilots complete')
    })
  }
})
router.post('/copyassets', auth.required, middlewares.isAdmin, function (req, res, next) {
  // if (req.err) {
  //   console.log(pe.render(new Error('Request Error')))
  //   console.log(pe.render(req.err))
  //   Mylogger.mylogger.SequelizeLogger.error(req.err.message)
  //   res.status(500)
  //   res.json({
  //     errorflag: true,
  //     message: req.err
  //   })
  //   res.end()
  // } else
  // if (req.user) {
  let packageData = null
  updateLastExport(req.body.pid, req.body.pilotid, 1).then(() => {
    return getFlaged(req.body.pid)
  }).then((data) => {
    return data
  }).then((data) => {
    packageData = data.dataValues
    return getPilot(req.body.pilotid)
  }).then((pilotData) => {
    let options = {
      debug: false, // remove console logs for copy
      overwrite: true,
      expand: true,
      dot: true,
      junk: true
    }
    return pilotcopy('../projects_dist/' + pilotData.pilot_foldername, '../projects_dist/' + packageData.package_folder, options)
  }).then(() => {
    res.status(200)
    res.json({
      errorflag: false,
      message: 'Assets Copied Succesfully',
      packageData: packageData
    })
    res.end()
    exportsLogger.info('/copyassets complete')
  }).catch((error) => {
    console.log(error)
    res.status(500)
    res.json({
      errorflag: true,
      message: error
    })
    res.end()
    exportsLogger.info('/copyassets fail ' + error)
  })
  // }
})
router.post('/createjson', middlewares.isAuthenticated, middlewares.isAdmin, function (req, res, next) {
  // if (req.err) {
  //   console.log(pe.render(new Error('Request Error')))
  //   console.log(pe.render(req.err))
  //   Mylogger.mylogger.SequelizeLogger.error(req.err.message)
  //   res.status(500)
  //   res.json({
  //     errorflag: true,
  //     message: req.err
  //   })
  //   res.end()
  // } else if (req.user) {
  let pagesData = null
  db.sequelize.query('SELECT pages.page_id, pages.page_name, pages.page_json, pages.module_id, pages.page_order, modules.module_order, pages.digi_activity_id, pages.digi_activity_name, pages.digi_module_id, pages.digi_module_name, pages.digi_skilltype_id, pages.digi_keywords, pages.digi_category, pages.digi_weight, pages.digi_eleclimitation, pages.digi_skilltype_name FROM pages INNER JOIN modules ON pages.module_id = modules.module_id WHERE pages.module_id IN (:modulestoexport) ORDER BY modules.module_order ASC, pages.page_order ASC', {
    replacements: {
      modulestoexport: req.body.modulestoexport
    },
    type: db.sequelize.QueryTypes.SELECT
  }).then((pages) => {
    pagesData = [...pages]
    return rimrafPromise('../projects_dist/' + req.body.packageFolder + '/src/assets/json/*')
  }).then(() => {
    return writeMultipleJsonPagesPromises(pagesData, req.body.packageFolder)
  }).then(() => {
    return createTocPromise(pagesData)
  }).then((toc) => {
    return writeFilePromise('../projects_dist/' + req.body.packageFolder + '/src/router/toc.json', toc)
  }).then(() => {
    return writeFilePromise('../projects_dist/' + req.body.packageFolder + '/src/assets/filterarray.json', req.body.filterarray)
  }).then(() => {
    res.status(200)
    res.json({
      errorflag: false,
      message: 'Json Created'
    })
    res.end()
    exportsLogger.info('/createjson complete')
  }).catch((error) => {
    console.log(error)
    res.status(500)
    res.json({
      errorflag: true,
      message: error
    })
    res.end()
    exportsLogger.info('/createjson fail ' + error)
  })
  // }
})
router.post('/emptyDist', auth.required, middlewares.isAdmin, function (req, res, next) {
  if (req.err) {
    console.log(pe.render(new Error('Request Error')))
    console.log(pe.render(req.err))
    Mylogger.mylogger.SequelizeLogger.error(req.err.message)
    res.status(500)
    res.json({
      errorflag: true,
      message: req.err
    })
    res.end()
  } else if (req.user) {
    rimrafPromise('../projects_dist/' + req.body.packageFolder + '/dist/*')
      .then(() => {
        res.status(200)
        res.json({
          errorflag: false,
          message: 'Dist is Empty'
        })
        res.end()
        exportsLogger.info('/empty dist complete')
      }).catch((error) => {
        console.log(error)
        res.status(500)
        res.json({
          errorflag: true,
          message: error
        })
        res.end()
        exportsLogger.info('/empty dist fail' + error)
      })
  }
})
io.on('connection', (socket) => {
  console.log('Socket Successful connection')
  socket.on('message', (response) => {
    if (response.message === 'build') {
      childProcessCommand('cd ../projects_dist/' + response.package + ' && npm run build', socket)
    } else {
      console.log(response)
    }
  })
  try {
    socket.emit('connection', {error: 'false', message: 'Connection Successful'})
  } catch (error) {
    exportsLogger.info('web socket connection' + error)
    throw new Error(error)
  }
})
router.post('/updatePackage', auth.required, middlewares.isAdmin, function (req, res, next) {
  // if (req.err) {
  //   console.log(pe.render(new Error('Request Error')))
  //   console.log(pe.render(req.err))
  //   Mylogger.mylogger.SequelizeLogger.error(req.err.message)
  //   res.status(500)
  //   res.json({
  //     errorflag: true,
  //     message: req.err
  //   })
  //   res.end()
  // } else if (req.user) {
  updateLastExport(req.body.packageId, req.body.exportid, 0, req.body.error)
    .then(() => {
      res.status(200)
      res.json({
        errorflag: false,
        message: 'Package Updated'
      })
      res.end()
      exportsLogger.info('/update package complete')
    }).catch((error) => {
      console.log(error)
      res.status(500)
      res.json({
        errorflag: true,
        message: error
      })
      res.end()
      exportsLogger.info('/update package fail' + error)
    })
  // }
})

router.post('/export', auth.required, middlewares.isAdmin, function (req, res, next) {
  if (req.err) {
    console.log(pe.render(new Error('Request Error')))
    console.log(pe.render(req.err))
    Mylogger.mylogger.SequelizeLogger.error(req.err.message)
    res.status(500)
    res.json({
      errorflag: true,
      message: req.err
    })
    res.end()
  } else if (req.user) {
    exportsLogger.info('STARTING export...')
    let packageid = req.body.pid
    let pilotfloder
    let folder
    let folderid
    let folder2
    let flag = true
    let toc = []
    db.packages.findOne({
      where: {
        id: packageid
      }
    }).then(data => {
      folder = data.dataValues.package_folder
      folderit = data.dataValues.id
    }).catch((err) => {
      Mylogger.mylogger.SequelizeLogger.error(err.message)
      console.log(pe.render(err))
    })
    db.pilotversions.findOne({
      where: {
        pilot_id: req.body.pilotid
      }
    })
      .then(pilot => {
        return pilot.get()
      }).then(data => {
        pilotfloder = data.pilot_foldername
        console.log('Ekana update ti vasi, paw gia copy')
        // pilotcopy(src, dest, [options], [callback])
        var options = {
          debug: false, // remove console logs for copy
          overwrite: true,
          expand: true,
          dot: true,
          junk: true
        }
        pilotcopy('../projects_dist/' + pilotfloder, '../projects_dist/' + folder, options).then(() => {
          console.log('copyed source folder!')
        }).then(() => {
          // 'SELECT pages.page_id, pages.page_name, pages.page_json, pages.module_id, pages.page_order, pages.digi_activity_id, pages.digi_activity_name, pages.digi_module_id, pages.digi_module_name, pages.digi_skilltype_id, pages.digi_keywords, pages.digi_category, pages.digi_weight, pages.digi_eleclimitation, pages.digi_skilltype_name, modules.module_id, modules.module_name, modules.module_order, packages.package_folder, packages.id FROM pages , modules , packages WHERE pages.module_id = modules.module_id AND modules.package_id = :pid AND modules.package_id = packages.id ORDER BY modules.module_order ASC, pages.page_order ASC'
          // SELECT pages.page_id, pages.page_name, pages.page_json, pages.module_id, pages.page_order, modules.module_order, pages.digi_activity_id, pages.digi_activity_name, pages.digi_module_id, pages.digi_module_name, pages.digi_skilltype_id, pages.digi_keywords, pages.digi_category, pages.digi_weight, pages.digi_eleclimitation, pages.digi_skilltype_name FROM pages INNER JOIN modules ON pages.module_id = modules.module_id WHERE pages.module_id IN (:modulestoexport) ORDER BY modules.module_order ASC, pages.page_order ASC
          db.sequelize.query('SELECT pages.page_id, pages.page_name, pages.page_json, pages.module_id, pages.page_order, modules.module_order, pages.digi_activity_id, pages.digi_activity_name, pages.digi_module_id, pages.digi_module_name, pages.digi_skilltype_id, pages.digi_keywords, pages.digi_category, pages.digi_weight, pages.digi_eleclimitation, pages.digi_skilltype_name FROM pages INNER JOIN modules ON pages.module_id = modules.module_id WHERE pages.module_id IN (:modulestoexport) ORDER BY modules.module_order ASC, pages.page_order ASC', {
            replacements: {
              modulestoexport: req.body.modulestoexport
            },
            type: db.sequelize.QueryTypes.SELECT
          }).then(pages => {
            let pname
            let pfile
            let tempname

            pilotfloder = data.package_folder

            // remove assets json folder before create a new one
            rimraf('../projects_dist/' + folder + '/src/assets/json/*', (err) => {
              if (err) {
                next(true, err)
              } else {
                console.log('delete folder')
                console.log('deleted assets json')
                // create json file for each of package pages
                pages.forEach(function (item, key) {
                  tempname = item.page_name.replace(/\s/g, '-')
                  pname = item.module_id + '-' + tempname
                  pfile = item.page_json
                  fs.writeFile('../projects_dist/' + folder + '/src/assets/json/' + pname + '.json', JSON.stringify(pfile, null, 4), (err) => {
                    if (err) {
                      Mylogger.mylogger.SequelizeLogger.error(err.message)
                      console.error(err)
                      return
                    };
                  })
                  fs.writeFile('../projects_dist/' + folder + '/src/assets/filterarray.json', JSON.stringify(req.body.filterarray, null, 4), (err) => {
                    if (err) {
                      Mylogger.mylogger.SequelizeLogger.error(err.message)
                      console.error(err)
                      return
                    };
                  })
                  toc.push({
                    url: pname,
                    order: key,
                    moduleid: item.module_id,
                    modulename: item.module_name,
                    module_order: item.module_order,
                    pagedata: pname + '.json',
                    pageorder: item.page_order,
                    digi_activity_id: item.digi_activity_id,
                    digi_activity_name: item.digi_activity_name,
                    digi_module_id: item.digi_module_id,
                    digi_module_name: item.digi_module_name,
                    digi_skilltype_id: item.digi_skilltype_id,
                    digi_keywords: item.digi_keywords,
                    digi_category: item.digi_category,
                    digi_weight: item.digi_weight,
                    digi_eleclimitation: item.digi_eleclimitation,
                    digi_skilltype_name: item.digi_skilltype_name
                  })
                })
                fs.writeFile('../projects_dist/' + folder + '/src/router/toc.json', JSON.stringify(toc, null, 2), (err) => {
                  if (err) {
                    Mylogger.mylogger.SequelizeLogger.error(err.message)
                    console.error(err)
                    return
                  };
                  res.status(200)
                  res.json({
                    errorflag: false,
                    message: 'Data copied successfully from DB/Pilot. Wait for optimization',
                    fold: folderit
                  })
                  res.end()
                  console.log('Teleiwsa to xtisimo. Paw gia build')
                })
              }
            })
          }).catch((err) => {
            Mylogger.mylogger.SequelizeLogger.error(err.message)
            console.log(pe.render(err))
          })
        }).catch((err) => {
          Mylogger.mylogger.SequelizeLogger.error(err.message)
          console.log(pe.render(err))
        })
      }).catch((err) => {
        Mylogger.mylogger.SequelizeLogger.error(err.message)
        console.log(pe.render(err))
      })
  }
  exportsLogger.info('FINISHED export')
})

router.post('/exportbuild', auth.required, middlewares.isAdmin, function (req, res, next) {
  if (req.err) {
    console.log(pe.render(new Error('Request Error')))
    console.log(pe.render(req.err))
    Mylogger.mylogger.SequelizeLogger.error(req.err.message)
    res.status(500)
    res.json({
      errorflag: true,
      message: req.err
    })
    res.end()
    exportsLogger.info('/exportbuild fail ' + req.err)
  } else if (req.user) {
    exportsLogger.info('STARTING export...')
    db.packages.findOne({
      where: {
        id: req.body.pilotid
      }
    })
      .then(pilot => {
        return pilot.get()
      }).then(data => {
        rimraf('../projects_dist/' + data.package_folder + '/dist/*', (err) => {
          if (err) {
            next(true, err)
            exportsLogger.info('rimram fail ' + err)
          } else {
            pilotfloder = data.package_folder
            let command = 'cd ../projects_dist/' + pilotfloder + ' && npm run build'
            console.log(pilotfloder)
            var cmd = exec(command)
            cmd.stdout.on('data', (data) => {
              console.log(`stdout: ${data}`)
            })

            cmd.stderr.on('data', (data) => {
              console.log(`stderr: ${data}`)
              res.status(500)
              res.json({
                errorflag: true,
                message: 'Error Export'
              })
              exportsLogger.info('/stderr fail ' + data)
            })

            cmd.on('close', (code) => {
              console.log(`child process exited with code ${code}`)
              getFlaged(req.body.pilotid).then(function (pid) {
                if (pid !== null) {
                  pid.update({
                    exportflag: 0,
                    lastexport: req.body.exportid
                  })
                }
              })
              res.status(200)
              res.json({
                errorflag: false,
                message: 'Thank god this was done! Now wait for the zip file.',
                foldername: pilotfloder
              })
            })
          }
        })
        exportsLogger.info('FINISHED export...')
      })
  }
})
router.post('/buildzip', middlewares.isAuthenticated, middlewares.isAdmin, function (req, res, next) {
  zipit('../projects_dist/' + req.body.packageFolder + '/dist', {
    saveTo: '../projects_dist/' + req.body.packageFolder + '/' + req.body.packageFolder + '.zip'
  }, function (err, buffer) {
    if (err === null) {
      res.status(200)
      res.json({
        errorflag: false,
        message: 'Zip was created. Download the file and upload it to Digipub (unzipp it, first)',
        folder: req.body.packageFolder
      })
      res.end()
      exportsLogger.info('/buildzip complete')
    } else {
      console.log(pe.render(err))
      Mylogger.mylogger.SequelizeLogger.error(err)
      res.status(500)
      res.json({
        errorflag: true,
        message: err
      })
      res.end()
      exportsLogger.info('/buildzip fail' + err)
    }
    console.log(buffer)
  })
}
)
router.post('/exportzip', auth.required, middlewares.isAdmin, function (req, res, next) {
  if (req.err) {
    console.log(pe.render(new Error('Request Error')))
    console.log(pe.render(req.err))
    Mylogger.mylogger.SequelizeLogger.error(req.err.message)
    res.status(500)
    res.json({
      errorflag: true,
      message: req.err
    })
    res.end()
    exportsLogger.info('/exportzip request fail' + err)
  } else if (req.user) {
    zipit('../projects_dist/' + pilotfloder + '/dist', {
      saveTo: '../projects_dist/' + pilotfloder + '/' + pilotfloder + '.zip',
      each: path => console.log(path, 'added!')
    }, function (err, buffer) {
      if (err === null) {
        res.status(200)
        res.json({
          errorflag: false,
          message: 'Zip was created. Download the file and upload it to Digipub (unzipp it, first)',
          folder: pilotfloder
        })
        res.end()
        exportsLogger.info('/exportzip complete')
        exportsLogger.info('FINISHED export of package #' + req.query.package)
      }
      exportsLogger.info('/exportzip fail' + err)
      console.log(err)
      console.log(buffer)
    })
  }
})

router.get('/downloadpck/:package', function (req, res, next) {
  var file = '../projects_dist/' + req.params.package + '/' + req.params.package + '.zip'
  res.type('application/zip').sendFile('projects_dist/' + req.params.package + '/' + req.params.package + '.zip', {
    root: '../'
  })
  // res.download(file, req.body.pilotid + 'zip')
})

module.exports = router
