var servicesContainer = require('../services/index')
const eventService = require('../services/eventService')

const bugController = {
  getBugs (req, res) {
    servicesContainer.bugService
      .getBugs()
      .then((allBugs) => {
        if (allBugs.length > 0) {
          res.status(200)
          res.json({
            error: false,
            message: 'Bugs list',
            bugs: allBugs
          })
        } else {
          res.status(200)
          res.json({
            error: false,
            message: 'No bugs found'
          })
        }
        res.end()
      })
      .catch((error) => {
        res.status(500)
        res.json({
          error: true,
          message: error
        })
      })
  },
  getBug (req, res) {
    // if (!req.params.id) {
    //   return res.status(400).json({
    //     error: true,
    //     message: 'Please give a bug id'
    //   })
    // }
    const bugIdToSearch = req.params.id
    servicesContainer.bugService
      .getBug(bugIdToSearch)
      .then((bug) => {
        if (bug) {
          return res.status(200).json({
            error: false,
            message: 'Found the bug',
            bug: bug
          })
        }
        return res.status(400).json({
          error: true,
          message: 'No such bug id'
        })
      })
      .catch((error) => {
        return res.status(500).json({
          error: true,
          message: error
        })
      })
  },
  addBug (req, res) {
    const toBeCreatedBug = req.body
    if (!toBeCreatedBug.package_id) {
      res.status(400).json({
        error: true,
        message: 'Please provide a package id'
      })
    }
    if (!toBeCreatedBug.module_id) {
      res.status(400).json({
        error: true,
        message: 'Please provide a module id'
      })
    }
    if (!toBeCreatedBug.page_id) {
      res.status(400).json({
        error: true,
        message: 'Please provide a page id'
      })
    }
    if (!toBeCreatedBug.component_name) {
      res.status(400).json({
        error: true,
        message: 'Please provide a component name'
      })
    }
    if (!toBeCreatedBug.bug_title) {
      res.status(400).json({
        error: true,
        message: 'Please provide a bug title'
      })
    }
    if (!toBeCreatedBug.bug_description) {
      res.status(400).json({
        error: true,
        message: 'Please provide a bug description'
      })
    }
    if (!toBeCreatedBug.bug_issuer) {
      res.status(400).json({
        error: true,
        message: 'Please provide the bug creator username'
      })
    }
    servicesContainer.bugService
      .addBug(toBeCreatedBug)
      .then(([bug, created]) => {
        if (created) {
          eventService.emit('bugEventSlack', bug)
          return res.status(200).json({
            error: false,
            message: 'Successfully created the bug',
            bug: bug
          })
        } else {
          res.status(409)
          res.send('Bug already exists')
          res.end()
        }
      })
      .catch((error) => {
        return res.status(500).json({
          error: true,
          message: error
        })
      })
  },
  async updateBug (req, res) {
    const toBeUpdatedBugID = req.params.id
    const toBeUpdatedBug = req.body
    try {
      let bug = await servicesContainer.bugService.updateBug(toBeUpdatedBugID, toBeUpdatedBug)
      if (bug) {
        eventService.emit('bugEventSlack', bug)
        return res.status(200).json({
          error: false,
          message: 'Successfully updated the bug',
          bug: bug
        })
      } else {
        return res.status(400).json({
          error: true,
          message: 'No such bug id'
        })
      }
    } catch (error) {
      return res.status(500).json({
        error: true,
        message: error
      })
    }
  },
  changeBugStatus (req, res) {
    const toBeUpdatedBug = req.body
    if (!toBeUpdatedBug.bug_id) {
      return res.status(400).json({
        error: true,
        message: 'Please provide a bug id'
      })
    }
    servicesContainer.bugService
      .changeBugStatus(toBeUpdatedBug.bug_id)
      .then((bug) => {
        if (bug) {
          return res.status(200).json({
            error: false,
            message: 'Successfully updated the bug status',
            bug: bug
          })
        }
        return res.status(400).json({
          error: true,
          message: 'No such bug id'
        })
      })
      .catch((error) => {
        return res.status(500).json({
          error: true,
          message: error
        })
      })
  },
  deleteBug (req, res) {
    const toBeDeletedBug = req.params.id
    if (!toBeDeletedBug) {
      return res.status(400).json({
        error: true,
        message: 'Please provide a bug id'
      })
    } else {
      servicesContainer.bugService
        .deleteBug(toBeDeletedBug)
        .then((flag) => {
          if (flag) {
            return res.status(200).json({
              error: false,
              message: 'Successfully deleted the bug'
            })
          }
          return res.status(400).json({
            error: true,
            message: 'No such bug id'
          })
        })
    }
  },
  paginateAndCount (req, res) {
    const {limit, offset} = req.body
    let count = servicesContainer.bugService.countAllBugs()
    let bugs = servicesContainer.bugService.findBugsWithPagination(limit, offset)
    Promise.all([
      count.catch(error => { throw error }),
      bugs.catch(error => { throw error })
    ]).then(values => {
      return res.status(200).json({
        error: false,
        message: 'Bugs list',
        count: values[0],
        bugs: values[1]
      })
    })
  },
  paginate (req, res) {
    const {limit, offset} = req.body
    servicesContainer.bugService.findBugsWithPagination(limit, offset)
      .then(bugs => {
        return res.status(200).json({
          error: false,
          message: 'Bugs list',
          bugs
        })
      })
      .catch((error) => {
        throw error
      })
  },
  searchPaginateAndCount (req, res) {
    const fieldToSearch = req.body.search_data
    const {offset, limit} = req.body
    let count = servicesContainer.bugService.searchFieldsCount(fieldToSearch)
    let bugs = servicesContainer.bugService.searchFieldsPagination(fieldToSearch, offset, limit)
    Promise.all([
      count.catch(error => { throw error }),
      bugs.catch(error => { throw error })
    ]).then(values => {
      if (!values[0]) {
        return res.status(200).json({
          error: false,
          message: 'No bugs found',
          count: values[0],
          bugs: values[1]
        })
      }
      return res.status(200).json({
        error: false,
        message: 'Bugs list',
        count: values[0],
        bugs: values[1]
      })
    }).catch((error) => {
      return res.status(500).json({
        error: true,
        message: error
      })
    })
  },
  searchPaginate (req, res) {
    const fieldToSearch = req.body.search_data
    const {offset, limit} = req.body
    servicesContainer.bugService.searchFieldsPagination(fieldToSearch, offset, limit)
      .then(bugs => {
        if (!bugs) {
          return res.status(200).json({
            error: false,
            message: 'No bugs found',
            bugs
          })
        }
        return res.status(200).json({
          error: false,
          message: 'Bugs list',
          bugs
        })
      }).catch((error) => {
        return res.status(500).json({
          error: true,
          message: error
        })
      })
  }
}
module.exports = bugController
