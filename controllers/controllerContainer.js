
// import userController from './userController'
// import authenticationController from './authenticationController'
// import bugController from './userController'
const userController = require('./userController')
const authenticationController = require('./authenticationController')
const bugController = require('./userController')

const controllerContainer = {
  bugController,
  userController,
  authenticationController
}
module.exports = controllerContainer
// export default controllerContainer
