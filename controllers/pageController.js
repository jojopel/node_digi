var servicesContainer = require('../services/')
var pe = require('../helpers/errorConfig')
const db = require('../models')
var _ = require('lodash')
var Mylogger = require('../server/main')

const pageController = {
  async getRecent (req, res) {
    const {limit} = req.body
    try {
      const recentPages = await servicesContainer.pageService.getRecent(req.user, limit)
      return res.status(200).json({
        error: false,
        message: 'Found pages',
        pages: recentPages
      })
    } catch (error) {
      return res.status(500).json({
        error: true,
        message: error
      })
    }
  },
  createPage (req, res) {
    var newpage = db.pages.build(req.body.data)
    newpage.page_order = 1
    db.pages.findOne({
      attributes: [
        [db.modules.sequelize.fn('MAX', db.pages.sequelize.col('page_order')), 'maxorder']
      ],
      where: {
        module_id: newpage.module_id
      }
    }).then((order) => {
      newpage.page_order = ++order.dataValues.maxorder
      newpage.created_by = req.user.user_id
      newpage.modified_by = req.user.user_id
      newpage.locked = req.user.user_id
      newpage.locked_by = req.user.user_id
      newpage.validate().then(() => {
        newpage.save().then(() => {
          return res.status(200).json({
            errorflag: false,
            message: 'Page Saved Succesfully',
            page: newpage
          })
        }).catch((err) => {
          console.log(pe.render(new Error('Save Page')))
          Mylogger.mylogger.SequelizeLogger.error(err.message)
          if (Object.keys(err).length === 0) {
            err = err.message
            return res.status(201).json({
              errorflag: true,
              message: err
            })
          } else {
            Mylogger.mylogger.SequelizeLogger.error(err.message)
            console.log(pe.render(err))
            return res.status(500).json({
              erroflag: true,
              message: err
            })
          }
        })
      }).catch((err) => {
        console.log(pe.render(err))
        Mylogger.mylogger.SequelizeLogger.error(err.message)
        return res.status(201).json({
          erroflag: true,
          message: err
        })
      })
    }).catch((err) => {
      console.log(pe.render(new Error('Find One Page')))
      console.log(pe.render(err))
      Mylogger.mylogger.SequelizeLogger.error(err.message)
    })
  },
  updatePage (req, res) {
    db.pages.findOne({
      where: {
        page_id: req.body.page_id
      }
    }).then((updatepage) => {
      if (!updatepage) {
        return res.status(201).json({
          errorflag: true,
          message: 'Page not found'
        })
      } else {
        updatepage.update({
          page_name: req.body.page_name
        }).then(() => {
          return res.status(200).json({
            errorflag: false,
            message: 'Page Updated Succesfully'
          })
        }).catch((err) => {
          console.log(pe.render(err))
          if (Object.keys(err).length === 0) {
            err = err.message
          }
          Mylogger.mylogger.SequelizeLogger.error(err.message)
          return res.status(201).json({
            errorflag: true,
            message: err
          })
        })
      }
    }).catch((err) => {
      console.log(pe.render(new Error('Find One Page')))
      console.log(pe.render(err))
      Mylogger.mylogger.SequelizeLogger.error(err.message)
      return res.status(500).json({
        errorflag: true,
        message: err
      })
    })
  },
  updateModule (req, res, next) {
    db.pages.findOne({
      where: {
        page_id: req.body.page_id
      }
    }).then((updatepage) => {
      if (!updatepage) {
        return res.status(201).json({
          errorflag: true,
          message: 'Page not found'
        })
      } else {
        db.pages.max('page_order', {
          where: {
            module_id: req.body.module_id
          }
        }).then((max) => {
          if (!max) {
            max = 0
          }
          updatepage.update({
            module_id: req.body.module_id,
            page_order: parseInt(max) + 1
          }).then(() => {
            return res.status(200).json({
              errorflag: false,
              message: 'Page Updated Succesfully'
            })
          }).catch((err) => {
            console.log(pe.render(err))
            if (Object.keys(err).length === 0) {
              err = err.message
            }
            Mylogger.mylogger.SequelizeLogger.error(err.message)
            return res.status(201).json({
              errorflag: true,
              message: err
            })
          })
        }).catch((err) => {
          console.log(pe.render(new Error('Update Module Page')))
          console.log(pe.render(err))
          Mylogger.mylogger.SequelizeLogger.error(err.message)
          return res.status(500).json({
            errorflag: true,
            message: err
          })
        })
      }
    }).catch((err) => {
      console.log(pe.render(new Error('Find One Page')))
      console.log(pe.render(err))
      Mylogger.mylogger.SequelizeLogger.error(err.message)
      return res.status(500).json({
        errorflag: true,
        message: err
      })
    })
  },
  updateJson (req, res) {
    db.pages.findOne({
      where: {
        page_id: req.body.data.page_id
      }
    }).then((updatepage) => {
      if (!updatepage) {
        return res.status(201).json({
          errorflag: true,
          message: 'Page not found'
        })
      } else {
        if (updatepage.locked !== 1) {
          updatepage.update({
            page_json: req.body.data.page_json,
            modified_by: req.user.user_id
          }).then(() => {
            res.status(200)
            res.json({
              errorflag: false,
              message: 'Page Json Updated Succesfully'
            })
            res.end()
          }).catch((err) => {
            console.log(pe.render(err))
            if (Object.keys(err).length === 0) {
              err = err.message
            }
            Mylogger.mylogger.SequelizeLogger.error(err.message)
            return res.status(201).json({
              errorflag: true,
              message: err
            })
          })
        } else if (updatepage.locked === 1 && req.user.user_id === updatepage.locked_by) {
          updatepage.update({
            page_json: req.body.data.page_json,
            modified_by: req.user.user_id
          }).then(() => {
            return res.status(200).json({
              errorflag: false,
              message: 'Page Json Updated Succesfully'
            })
          }).catch((err) => {
            console.log(pe.render(err))
            if (Object.keys(err).length === 0) {
              err = err.message
            }
            Mylogger.mylogger.SequelizeLogger.error(err.message)
            return res.status(201).json({
              errorflag: true,
              message: err
            })
          })
        } else {
          return res.status(201).json({
            errorflag: true,
            message: 'Page is currently locked for edit by another user. Please try again later '
          })
        }
      }
    }).catch((err) => {
      console.log(pe.render(new Error('Find One Page')))
      console.log(pe.render(err))
      Mylogger.mylogger.SequelizeLogger.error(err.message)
      return res.status(500).json({
        errorflag: true,
        message: err
      })
    })
  },
  findPageById (req, res) {
    db.pages.findOne({
      include: [{
        model: db.users,
        as: 'pagelockedbyuser',
        attributes: ['username'],
        distinct: true
      },
      {
        model: db.modules,
        attributes: ['module_id', 'module_name'],
        include: [{
          model: db.packages,
          attributes: ['package_name', 'id']
        }],
        distinct: true
      }],
      attributes: ['page_id', 'page_name', 'locked_by', 'digi_activity_id', 'digi_activity_name', 'digi_module_id', 'digi_module_name', 'digi_skilltype_id', 'digi_keywords', 'digi_category', 'digi_weight', 'digi_eleclimitation'],
      where: {
        page_id: req.params.id
      }
    }).then((page) => {
      if (!page) {
        return res.status(201).json({
          errorflag: true,
          message: 'Page not found'
        })
      } else if (page) {
        if (page.dataValues.locked_by === req.user.user_id) {
          page.dataValues.pagelockedbyuser.username = 'Me'
        }
        return res.status(200).json({
          errorflag: false,
          message: 'Page found',
          page: page
        })
      }
    }).catch((err) => {
      console.log(pe.render(new Error('Find One Page')))
      console.log(pe.render(err))
      Mylogger.mylogger.SequelizeLogger.error(err.message)
      return res.status(500).json({
        errorflag: true,
        message: err
      })
    })
  },
  assignPage (req, res) {
    db.pages.update(
      {
        page_name: req.body.data.page_name,
        page_url: req.body.data.page_url,
        digi_skilltype_id: req.body.data.page_skilltype_id,
        digi_skilltype_name: req.body.data.page_skilltype_name,
        digi_activity_id: req.body.data.page_activity_id,
        digi_activity_name: req.body.data.page_activity_name,
        digi_module_id: req.body.data.page_module_id,
        digi_module_name: req.body.data.page_module_name,
        digi_keywords: req.body.data.page_keywords,
        digi_category: req.body.data.page_category,
        digi_weight: req.body.data.page_weight,
        digi_eleclimitation: req.body.data.page_eleclimitation
      },
      {
        where: {
          page_id: req.body.data.page_id
        }
      }
    ).then((response) => {
      if (response[0] !== 1) {
        return res.status(201).json({
          errorflag: true,
          message: 'Page not found'
        })
      } else {
        return res.status(200).json({
          errorflag: false,
          message: 'Page Updated Succesfully'
        })
      }
    }).catch((err) => {
      Mylogger.mylogger.SequelizeLogger.error(err.message)
      return res.status(500).json({
        errorflag: true,
        message: err
      })
    })
  },
  nextPage (req, res) {
    db.pages.findAll({
      include: {
        model: db.users,
        as: 'pagelockedbyuser',
        attributes: ['username'],
        distinct: true
      },
      where: {
        module_id: req.body.data.module_id
      }
    }).then((page) => {
      if (!page) {
        return res.status(201).json({
          errorflag: true,
          message: 'Pages not found'
        })
      } else if (page) {
        let temparray = _.map(page, 'dataValues')
        temparray = _.orderBy(temparray, ['page_order'], ['asc'])
        let currentpage = temparray[_.findIndex(temparray, { 'page_id': parseInt(req.body.data.page_id) })]
        let updatepage = page[_.findIndex(page, { 'page_id': parseInt(req.body.data.page_id) })]
        // Decline 2 var as helpers templocked and templockedBy so update through them the current page db record
        let templocked = null
        let templockedBy = null
        // First check if this page is locked by logged in user
        if (currentpage.locked_by === req.user.user_id) {
          // Check if is the last page of the module so keep it locked for the logged in user
          if (req.body.data.navbtn === 'next' && temparray[(_.indexOf(temparray, currentpage) + 1)] === undefined) {
            templocked = currentpage.locked
            templockedBy = currentpage.locked_by
            // Check if is the first page of the module so keep it locked for the logged in user
          } else if (req.body.data.navbtn === 'previous' && temparray[(_.indexOf(temparray, currentpage) - 1)] === undefined) {
            templocked = currentpage.locked
            templockedBy = currentpage.locked_by
            // If page is not locked for another user and is not the last or the first page of the module unlocked it
          } else {
            templocked = 0
            templockedBy = 0
          }
          // If page is locked for another user just updated with the same values as it is
        } else {
          templocked = currentpage.locked
          templockedBy = currentpage.locked_by
        }
        updatepage.update({
          locked: templocked,
          locked_by: templockedBy
        }).then(() => {
          let nextpage
          let templock = null
          if (req.body.data.navbtn === 'next') {
            nextpage = temparray[(_.indexOf(temparray, currentpage) + 1)]
            if (nextpage !== undefined) {
              let updatenextpage = page[_.findIndex(page, { 'page_id': nextpage.page_id })]
              // use it to temp lock the page if is unlocked or let it locked for the user that is already locked on
              if (nextpage.locked_by === 0) {
                templock = req.user.user_id
              } else {
                templock = nextpage.locked_by
              }
              updatenextpage.update({
                locked: 1,
                locked_by: templock
              }).then(() => {
                if (nextpage.pagelockedbyuser === null) {
                  nextpage.pagelockedbyuser = {
                    username: 'Me'
                  }
                } else if (nextpage.locked_by === req.user.user_id) {
                  nextpage.pagelockedbyuser = {
                    username: 'Me'
                  }
                }
                return res.status(200).json({
                  errorflag: false,
                  message: 'Page found',
                  page: nextpage
                })
              }).catch((err) => {
                Mylogger.mylogger.SequelizeLogger.error(err.message)
                console.log(pe.render(err))
                return res.status(201).json({
                  errorflag: true,
                  message: err
                })
              })
            } else {
              if (currentpage.pagelockedbyuser === null) {
                currentpage.pagelockedbyuser = {
                  username: 'Me'
                }
              } else if (currentpage.locked_by === req.user.user_id) {
                currentpage.pagelockedbyuser = {
                  username: 'Me'
                }
              }
              return res.status(201).json({
                errorflag: true,
                message: 'There is no more pages in this module',
                page: currentpage
              })
            }
          } else if (req.body.data.navbtn === 'previous') {
            nextpage = temparray[(_.indexOf(temparray, currentpage) - 1)]
            if (nextpage !== undefined) {
              let updatepreviouspage = page[_.findIndex(page, { 'page_id': nextpage.page_id })]
              // use it to temp lock the page if is unlocked or let it locked for the user that is already locked on
              if (nextpage.locked_by === 0) {
                templock = req.user.user_id
              } else {
                templock = nextpage.locked_by
              }
              updatepreviouspage.update({
                locked: 1,
                locked_by: templock
              }).then(() => {
                if (nextpage.pagelockedbyuser === null) {
                  nextpage.pagelockedbyuser = {
                    username: 'Me'
                  }
                } else if (nextpage.locked_by === req.user.user_id) {
                  nextpage.pagelockedbyuser = {
                    username: 'Me'
                  }
                }
                return res.status(200).json({
                  errorflag: false,
                  message: 'Page found',
                  page: nextpage
                })
              }).catch((err) => {
                Mylogger.mylogger.SequelizeLogger.error(err.message)
                console.log(pe.render(err))
                return res.status(201).json({
                  errorflag: true,
                  message: err
                })
              })
            } else {
              if (currentpage.pagelockedbyuser === null) {
                currentpage.pagelockedbyuser = {
                  username: 'Me'
                }
              } else if (currentpage.locked_by === req.user.user_id) {
                currentpage.pagelockedbyuser = {
                  username: 'Me'
                }
              }
              return res.status(201).json({
                errorflag: true,
                message: 'There is no more pages in this module',
                page: currentpage
              })
            }
          }
        }).catch((err) => {
          console.log(pe.render(new Error('Update Page Error')))
          Mylogger.mylogger.SequelizeLogger.error(err.message)
          console.log(pe.render(err))
          return res.status(500).json({
            errorflag: true,
            message: err
          })
        })
      }
    }).catch((err) => {
      console.log(pe.render(new Error('Find All Pages')))
      console.log(pe.render(err))
      Mylogger.mylogger.SequelizeLogger.error(err.message)
      return res.status(500).json({
        errorflag: true,
        message: err
      })
    })
  },
  getModulePages (req, res) {
    db.pages.findAll({
      distinct: true,
      include: [{
        as: 'page_modifiedby',
        model: db.users,
        attributes: ['username']
      },
      {
        as: 'page_createdby',
        model: db.users,
        attributes: ['username']
      },
      {
        model: db.modules,
        attributes: ['module_id', 'module_name']
      }],
      attributes: ['page_id', 'module_id', 'page_name', 'page_order', 'updatedAt', 'createdAt', 'modified_by', 'created_by', 'digi_activity_id', 'digi_activity_name', 'digi_module_id', 'digi_module_name', 'digi_skilltype_id', 'digi_skilltype_name'],
      where: {
        module_id: req.body.id
      },
      order: ['page_order'],
      limit: req.body.paginationlimit,
      offset: req.body.paginationoffset
    }).then((pages) => {
      return res.status(200).json({
        errorflag: false,
        pages: pages
      })
    }).catch((err) => {
      console.log(pe.render(new Error('Find All Pages')))
      console.log(pe.render(err))
      Mylogger.mylogger.SequelizeLogger.error(err.message)
      return res.status(500).json({
        errorflag: true,
        message: err
      })
    })
  },
  unlockPage (req, res) {
    db.pages.findOne({
      where: {
        page_id: req.body.data.page_id
      }
    }).then((page) => {
      page.update({
        locked: 1,
        locked_by: req.user.user_id
      }).then((page) => {
        return res.status(200).json({
          errorflag: false,
          message: 'Page Unlocked Succesfully',
          username: 'Me'
        })
      }).catch((err) => {
        console.log(pe.render(new Error('Update Page Error')))
        console.log(pe.render(err))
        Mylogger.mylogger.SequelizeLogger.error(err.message)
        return res.status(500).json({
          errorflag: true,
          message: err
        })
      })
    }).catch((err) => {
      console.log(pe.render(new Error('Find One Page')))
      console.log(pe.render(err))
      Mylogger.mylogger.SequelizeLogger.error(err.message)
      return res.status(500).json({
        errorflag: true,
        message: err
      })
    })
  },
  lockPage (req, res) {
    db.pages.findAll({
      where: {
        locked_by: req.user.user_id
      }
    }).then((pages) => {
      var updatearray = []
      for (var i = 0; i < pages.length; i++) {
        var updatepage = pages[i].update({
          locked: 0,
          locked_by: 0
        })
        updatearray.push(updatepage)
      }
      var promises = Promise.all(updatearray)
      promises.then(() => {
        db.pages.findOne({
          include: [{
            model: db.users,
            as: 'pagelockedbyuser',
            attributes: ['username'],
            distinct: true
          },
          {
            model: db.modules,
            attributes: ['module_id', 'module_name'],
            include: [{
              model: db.packages,
              attributes: ['package_name', 'id']
            }],
            distinct: true
          }],
          attributes: ['page_id', 'page_name', 'locked_by', 'module_id', 'page_json'],
          where: {
            page_id: req.body.data.page_id
          }
        }).then((page) => {
          if (page.dataValues.locked_by === 0 || page.dataValues.locked_by === req.user.user_id) {
            page.update({
              locked: 1,
              locked_by: req.user.user_id
            }, {
              returning: true
            }).then((page) => {
              return res.status(200).json({
                errorflag: false,
                message: 'Page Locked Succesfully',
                page: page
              })
            }).catch((err) => {
              console.log(pe.render(err))
              if (Object.keys(err).length === 0) {
                err = err.message
              }
              Mylogger.mylogger.SequelizeLogger.error(err.message)
              return res.status(201).json({
                errorflag: true,
                message: err
              })
            })
          } else {
            return res.status(200).json({
              errorflag: false,
              message: 'Page Already Locked',
              page: page
            })
          }
        }).catch((err) => {
          console.log(pe.render(new Error('Find One Page')))
          console.log(pe.render(err))
          Mylogger.mylogger.SequelizeLogger.error(err.message)
          return res.status(500).json({
            errorflag: true,
            message: err
          })
        })
      }).catch((err) => {
        console.log(pe.render(new Error('Update All Promises')))
        console.log(pe.render(err))
        Mylogger.mylogger.SequelizeLogger.error(err.message)
        if (Object.keys(err).length === 0) {
          err = err.message
          return res.status(201).json({
            errorflag: true,
            message: err
          })
        } else {
          return res.status(500).json({
            errorflag: true,
            message: err
          })
        }
      })
    }).catch((err) => {
      console.log(pe.render(new Error('Find All Pages')))
      console.log(pe.render(err))
      Mylogger.mylogger.SequelizeLogger.error(err.message)
      return res.status(500).json({
        errorflag: true,
        message: err
      })
    })
  },
  insertPage (req, res) {
    var newpagearray = []
    var validpromises = []
    var savepromises = []
    req.body.forEach(function (item) {
      if (item.page_id) {
        db.pages.findOne({
          where: {
            page_id: item.page_id
          }
        }).then((updatepage) => {
          updatepage.update({
            page_order: item.page_order,
            modified_by: req.user.user_id
          })
        })
      } else {
        var newpage = db.pages.build({
          module_id: item.module_id,
          page_name: item.page_name,
          page_order: item.page_order,
          created_by: req.user.user_id,
          modified_by: req.user.user_id,
          locked: 0,
          locked_by: 0
        })
        newpagearray.push({
          newpage: newpage
        })
      }
    })
    for (var i = 0; i < newpagearray.length; i++) {
      var validpromise = newpagearray[i].newpage.validate()
      validpromises.push(validpromise)
    }
    Promise.all(validpromises).then(() => {
      for (var j = 0; j < newpagearray.length; j++) {
        var savepromise = newpagearray[j].newpage.save()
        savepromises.push(savepromise)
      }
      Promise.all(savepromises).then(() => {
        return res.status(200).json({
          errorflag: false,
          message: 'All Pages Saved Succesfully'
        })
      }).catch((err) => {
        console.log(pe.render(new Error('Save All Pages Promises')))
        Mylogger.mylogger.SequelizeLogger.error(err.message)
        if (Object.keys(err).length === 0) {
          return res.status(201).json({
            errorflag: true,
            message: err.message
          })
        } else {
          return res.status(500).json({
            errorflag: true,
            message: err
          })
        }
      })
    }).catch((err) => {
      Mylogger.mylogger.SequelizeLogger.error(err.message)
      console.log(pe.render(err))
      return res.status(201).json({
        errorflag: true,
        message: err
      })
    })
  },
  deletePage (req, res) {
    deletePage(req.body.id).then((response) => {
      return res.status(response.status).json({
        errorflag: response.errorflag,
        message: response.message
      })
    }).catch((err) => {
      console.log(pe.render(new Error('Delete Page')))
      Mylogger.mylogger.SequelizeLogger.error(err.message)
      console.log(pe.render(err))
      return res.status(500).json({
        errorflag: true,
        message: err
      })
    })
  },
  countPages (req, res) {
    db.pages.count({
      where: {
        module_id: req.body.id
      }
    }).then((count) => {
      return res.status(200).json({
        errorflag: false,
        count: count
      })
    }).catch(err => {
      console.log(pe.render(new Error('Count All Pages')))
      Mylogger.mylogger.SequelizeLogger.error(err.message)
      console.log(pe.render(err))
      return res.status(500).json({
        errorflag: true,
        message: err
      })
    })
  },
  myRecentPages (req, res) {
    db.pages.findAll({
      include: [{
        model: db.modules,
        attributes: ['module_id'],
        include: [{
          model: db.packages,
          attributes: ['package_name', 'id']
        }],
        distinct: false
      }],
      where: {
        modified_by: req.user.user_id
      },
      order: [[db.Sequelize.col('updatedAt'), 'DESC']],
      limit: req.body.paginationlimit,
      offset: req.body.paginationoffset
    }).then((pages) => {
      if (pages.length < 1) {
        return res.status(201).json({
          errorflag: true,
          message: 'Pages not Found'
        })
      } else {
        return res.status(200).json({
          errorflag: false,
          pages: pages
        })
      }
    }).catch((error) => {
      console.log(pe.render(new Error('Find Recent Pages')))
      Mylogger.mylogger.SequelizeLogger.error(error.message)
      console.log(pe.render(error))
      return res.status(500).json({
        errorflag: true,
        message: error
      })
    })
  }
}

/**
 * find page
 * if page assigned to digibooks, not allowed delete
 * else
 * delete page
 * re order rest pages
 * @param {*} pageID
 */
const deletePage = (pageID) => {
  var rowDeleted = {}
  return new Promise((resolve, reject) => {
    db.pages.findOne({ where: { page_id: pageID } }).then((deletedPage) => {
      if (!deletedPage) {
        resolve({ errorflag: true, status: 204, message: 'Page not Found' })
      } else if (deletedPage.dataValues.digi_activity_id !== null) {
        resolve({ errorflag: true, status: 203, message: 'Not Allowed, Page is already Assigned to ExpressDigiBooks' })
      } else {
        rowDeleted = JSON.parse(JSON.stringify(deletedPage))
        return deletedPage.destroy()
      }
    }).then(() => {
      return db.pages.findAll({
        where: {
          $and: [{
            page_order: {
              $gt: rowDeleted.page_order
            },
            module_id: rowDeleted.module_id
          }]
        }
      })
    }).then((pages) => {
      return Promise.all(
        pages.map((page) => {
          return page.decrement(['page_order'], {
            by: 1
          })
        })
      )
    }).then(() => {
      resolve({ errorflag: false, status: 200, message: 'Page Deleted Succesfully' })
    }).catch((error) => {
      reject(error)
    })
  })
}

module.exports = pageController
