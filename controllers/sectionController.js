const db = require('../models')
var Mylogger = require('../server/main')
var pe = require('../helpers/errorConfig')

const sectionController = {
  createSection (req, res) {
    var newsection = db.sections.build(req.body.data.form)
    newsection.created_by = req.user.user_id
    newsection.modified_by = req.user.user_id
    newsection.validate().then(() => {
      newsection.save().then(() => {
        db.sectionstopackages.findOrCreate({
          where: {
            $and: [{
              section_id: newsection.section_id
            }, {
              package_id: req.body.data.package_id
            }]
          },
          defaults: {
            section_id: newsection.section_id,
            package_id: req.body.data.packageid
          }
        }).then(() => {
          return res.status(200).json({
            errorflag: false,
            message: 'Section Saved Succesfully'
          })
        }).catch((err) => {
          console.log(pe.render(new Error('Assigned New Section')))
          console.log(pe.render(err))
          Mylogger.mylogger.SequelizeLogger.error(err.message)
          return res.status(500).json({
            erroflag: false,
            message: err
          })
        })
      }).catch((err) => {
        console.log(pe.render(new Error('Save Sections')))
        console.log(pe.render(err))
        Mylogger.mylogger.SequelizeLogger.error(err.message)
        return res.status(500).json({
          erroflag: false,
          message: err
        })
      })
    }).catch((err) => {
      Mylogger.mylogger.SequelizeLogger.error(err.message)
      console.log(pe.render(err))
      return res.status(201).json({
        errorflag: true,
        message: err
      })
    })
  },
  updateSection (req, res) {
    db.sections.findOne({
      where: {
        'section_id': req.body.data.section_id
      }
    }).then((updatesection) => {
      if (!updatesection) {
        return res.status(201).json({
          errorflag: true,
          message: 'Section not found'
        })
      } else {
        updatesection.update({
          section_name: req.body.data.section_name,
          section_category: req.body.data.section_category,
          section_area: req.body.data.section_area,
          modified_by: req.user.user_id
        }).then(() => {
          return res.status(200).json({
            errorflag: false,
            message: 'Section Updated Succesfully'
          })
        }).catch((err) => {
          Mylogger.mylogger.SequelizeLogger.error(err.message)
          console.log(pe.render(err))
          return res.status(201).json({
            errorflag: true,
            message: err
          })
        })
      }
    }).catch((err) => {
      console.log(pe.render(new Error('Find One Section')))
      console.log(pe.render(err))
      Mylogger.mylogger.SequelizeLogger.error(err.message)
      return res.status(500).json({
        errorflag: true,
        message: err
      })
    })
  },
  findSectionById (req, res) {
    db.sections.findOne({
      where: {
        section_id: req.params.id
      }
    }).then((findsection) => {
      return res.status(200).json({
        errorflag: false,
        section: findsection
      })
    }).catch((err) => {
      console.log(pe.render(new Error('Find One Section')))
      console.log(pe.render(err))
      Mylogger.mylogger.SequelizeLogger.error(err.message)
      res.status(500).json({
        errorflag: true,
        message: err
      })
    })
  },
  getAssigned (req, res) {
    db.sectionstopackages.destroy({
      where: {
        package_id: req.body.data.packageid
      }
    }).then(() => {
      let temparray = JSON.parse(req.body.data.jsondata)
      var findorcreatearray = []
      for (var i = 0; i < temparray.length; i++) {
        var newassigned = db.sectionstopackages.build({ section_id: temparray[i].section_id, package_id: temparray[i].package_id })
        var findorcreate = newassigned.save()
        findorcreatearray.push(findorcreate)
      }
      var promises = Promise.all(findorcreatearray)
      promises.then(() => {
        return res.status(200).json({
          errorflag: false,
          message: 'All Sections Assigned to Package'
        })
      }).catch((err) => {
        console.log(pe.render(new Error('Find Or Create Promises Sections')))
        console.log(pe.render(err))
        Mylogger.mylogger.SequelizeLogger.error(err.message)
        if (Object.keys(err).length === 0) {
          err = err.message
          return res.status(201).json({
            errorflag: true,
            message: err
          })
        } else {
          return res.status(500).json({
            errorflag: true,
            message: err
          })
        }
      })
    }).catch((err) => {
      console.log(pe.render(new Error('Find Or Create Promises Sections')))
      console.log(pe.render(err))
      Mylogger.mylogger.SequelizeLogger.error(err.message)
      if (Object.keys(err).length === 0) {
        err = err.message
        return res.status(201).json({
          errorflag: true,
          message: err
        })
      } else {
        return res.status(500).json({
          errorflag: true,
          message: err
        })
      }
    })
  },
  deleteSection (req, res) {
    db.sectionstopackages.destroy({
      where: {
        'package_id': req.body.data
      }
    }).then((sections) => {
      return res.status(200).json({
        errorflag: false,
        message: 'All Section Package dependencies have been deleted'
      })
    }).catch(err => {
      console.log(pe.render(new Error('Destroy Sections')))
      console.log(pe.render(err))
      Mylogger.mylogger.SequelizeLogger.error(err.message)
      return res.status(500).json({
        errorflag: true,
        message: err
      })
    })
  },
  findSectionsByCategory (req, res) {
    db.sections.findAll({
      where: {
        'section_category': req.body.data
      },
      include: [{
        model: db.users,
        as: 'creator_role',
        attributes: ['role_id'],
        distinct: true
      }],
      // group: ['creator_role.role_id'],
      order: [
        ['section_name', 'ASC']
      ]
    }).then((sections) => {
      return res.status(200).json({
        errorflag: false,
        sections: sections
      })
    }).catch(err => {
      console.log(pe.render(new Error('Find All Sections')))
      console.log(pe.render(err))
      Mylogger.mylogger.SequelizeLogger.error(err.message)
      return res.status(500).json({
        errorflag: true,
        message: err
      })
    })
  },
  findSectionsByCategoryOrAll (req, res) {
    if (req.body.data !== undefined) {
      db.sections.findAll({
        include: [{
          model: db.packages,
          as: 'sectionpackage',
          attributes: ['id'],
          where: {
            'id': req.body.data
          },
          distinct: true
        }, {
          model: db.packagecategories,
          attributes: ['category_name']
        }, {
          model: db.sectionareas,
          attributes: ['area_name']
        }, {
          model: db.sectiongroups,
          attributes: ['sectiongroup_name']
        }]
      }).then((sections) => {
        if (sections.length > 0) {
          return res.status(200).json({
            errorflag: false,
            sections: sections
          })
        } else {
          return res.status(201).json({
            errorflag: true,
            message: 'Sections have not been assigned to this package'
          })
        }
      }).catch(err => {
        console.log(pe.render(new Error('Find All Sections')))
        console.log(pe.render(err))
        return res.status(500).json({
          errorflag: true,
          message: err
        })
      })
    } else if (req.body.data === undefined) {
      db.sections.findAll({
        include: [{
          model: db.packagecategories,
          attributes: ['category_name']
        }, {
          model: db.sectionareas,
          attributes: ['area_name']
        }, {
          model: db.sectiongroups,
          attributes: ['sectiongroup_name']
        }],
        raw: true
      }).then((sections) => {
        res.status(200).json({
          errorflag: false,
          sections: sections
        })
      }).catch(err => {
        console.log(pe.render(new Error('Find All Sections')))
        console.log(pe.render(err))
        Mylogger.mylogger.SequelizeLogger.error(err.message)
        return res.status(500).json({
          errorflag: true,
          message: err
        })
      })
    } else {
      return res.status(200).json({
        errorflag: false,
        message: 'You are authenticated'
      })
    }
  },
  getGroups (req, res) {
    db.sectiongroups.findAll({
      attributes: ['sectiongroup_id', 'sectiongroup_name']
    }).then((groups) => {
      return res.status(200).json({
        errorflag: false,
        groups: groups
      })
    }).catch(err => {
      console.log(pe.render(new Error('Find All Groups')))
      console.log(pe.render(err))
      Mylogger.mylogger.SequelizeLogger.error(req.err.message)
      res.status(500).json({
        errorflag: true,
        message: err
      })
    })
  },
  getAreas (req, res) {
    db.sectionareas.findAll({
      attributes: ['area_id', 'area_name']
    }).then((areas) => {
      return res.status(200).json({
        errorflag: false,
        areas: areas
      })
    }).catch(err => {
      console.log(pe.render(new Error('Find All Areas')))
      console.log(pe.render(err))
      Mylogger.mylogger.SequelizeLogger.error(err.message)
      return res.status(500).json({
        errorflag: true,
        message: err
      })
    })
  },
  getMySections (req, res) {
    db.sections.findAll({
      attributes: ['section_id', 'section_name', 'created', 'section_category', 'section_area', 'section_group', 'section_json'],
      include: [{
        model: db.packagecategories,
        attributes: ['category_name']
      }, {
        model: db.sectionareas,
        attributes: ['area_name']
      }, {
        model: db.sectiongroups,
        attributes: ['sectiongroup_name']
      }, {
        model: db.packages,
        as: 'sectionpackage',
        attributes: ['package_name']
      }],
      where: {
        'created_by': req.user.user_id
      },
      order: [
        ['created', 'ASC']
      ]
    }).then((sections) => {
      return res.status(200).json({
        errorflag: false,
        sections: sections
      })
    }).catch((err) => {
      console.log(pe.render(new Error('Find My Sections')))
      console.log(pe.render(err))
      Mylogger.mylogger.SequelizeLogger.error(err.message)
      return res.status(500).json({
        errorflag: true,
        message: err
      })
    })
  },
  deleteSection2 (req, res) {
    db.sectionstopackages.destroy({
      where: {
        'section_id': req.body.section_id
      }
    }).then((response) => {
      db.sections.destroy({
        where: {
          'section_id': req.body.section_id
        }
      }).then((sections) => {
        return res.status(200).json({
          errorflag: false,
          message: 'Section have been deleted'
        })
      }).catch(err => {
        console.log(pe.render(new Error('Delete Section')))
        console.log(pe.render(err))
        Mylogger.mylogger.SequelizeLogger.error(err.message)
        return res.status(500).json({
          errorflag: true,
          message: err
        })
      })
    }).catch(err => {
      console.log(pe.render(new Error('Delete Section Dependencies')))
      console.log(pe.render(err))
      Mylogger.mylogger.SequelizeLogger.error(err.message)
      return res.status(500).json({
        errorflag: true,
        message: err
      })
    })
  },
  sectionMenu (req, res) {
    db.sections.findAll({
      attributes: ['section_id', 'section_name', 'section_category', 'section_area', 'section_group'],
      include: [{
        model: db.packagecategories,
        attributes: ['category_name']
      }, {
        model: db.sectionareas,
        attributes: ['area_name']
      }, {
        model: db.sectiongroups,
        attributes: ['sectiongroup_name']
      }, {
        model: db.packages,
        as: 'sectionpackage',
        attributes: ['package_name', 'id'],
        where: {
          'id': req.body.data
        }
      }],
      order: [
        ['created', 'ASC']
      ]
    }).then((sections) => {
      return res.status(200).json({
        errorflag: false,
        sections: sections
      })
    }).catch(err => {
      console.log(pe.render(new Error('Find All Sections Menu')))
      console.log(pe.render(err))
      Mylogger.mylogger.SequelizeLogger.error(err.message)
      res.status(500).json({
        errorflag: true,
        message: err
      })
    })
  }

}

module.exports = sectionController
