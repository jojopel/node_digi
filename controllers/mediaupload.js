var router = require('express').Router()
var pe = require('../helpers/errorConfig')
var auth = require('../controllers/auth.js')
const db = require('../models')
var multipart = require('connect-multiparty')
var multipartMiddleware = multipart()
var uploader = require('../controllers/uploader-node.js')('../tmp')
var fs = require('fs')
require('events').EventEmitter.prototype._maxListeners = 100
var path = require('path')
// var thumb = require('../controllers/thumbnail.js').thumb
// var jimp = require('jimp')
var mkdirp = require('mkdirp')
var ACCESS_CONTROLL_ALLOW_ORIGIN = true
// Handle uploads through Uploader.js

db.media.getAll = function (pid) {
  return db.media.findAll({
    include: {
      model: db.packages,
      attributes: ['package_folder', 'id'],
      distinct: true
    },
    where: {
      package_id: pid
    }
  })
}
db.media.getOne = function (flnmae, pid) {
  return new Promise((resolve, reject) => {
    db.media.findOne({
      include: {
        model: db.packages,
        attributes: ['package_folder', 'id'],
        distinct: true
      },
      where: {
        filename: flnmae,
        package_id: pid
      }
    }).then((media) => {
      if (media) {
        resolve(media)
      } else {
        reject('no media found' + media)
      }
    }).catch((error) => {
      reject('no media found' + error)
    })
  })
}
db.media.getThumb = function (flnmae, pid) {
  return db.media.findOne({
    include: {
      model: db.packages,
      attributes: ['package_folder', 'id'],
      distinct: true
    },
    where: {
      thumb: flnmae,
      package_id: pid
    }
  })
}
router.post('/upload', auth.required, multipartMiddleware, function (req, res) {
  uploader.post(req, function (status, filename, original_filename, identifier) {
    // if (ACCESS_CONTROLL_ALLOW_ORIGIN) {
    //   res.header('Access-Control-Allow-Origin', '*')
    // }
    if (status === 'done') {
      db.packages.findAll({
        limit: 1,
        attributes: ['package_folder', 'id'],
        where: {
          id: req.body.package
        }
      }).then(function (entries) {
        let packagefolderid = entries.map(entries => entries.id)
        let packagefolder = entries.map(entries => entries.package_folder)
        mkdirp('../projects_dist/' + packagefolder[0] + '/src/assets/media/', function (err) {
          if (err) console.error(err)
          var s = fs.createWriteStream('../projects_dist/' + packagefolder[0] + '/src/assets/media/' + filename)
          uploader.write(identifier, s, {
            end: true
          })
          s.on('finish', function () {
            uploader.clean(identifier) // prepei na uparxei so onfinish clean tmp chuncks
            db.media.findOrCreate({
              where: {
                $and: [{
                  package_id: packagefolderid[0]
                }, {
                  filename: filename
                }]
              },
              defaults: {
                package_id: packagefolderid[0],
                filename: filename,
                media_type: req.files.file.type,
                created_by: req.user.user_id,
                modified_by: req.user.user_id
              }
            }).then(function (user) {
              res.status(200)
            })
          })
        })
      })
    }
    setTimeout(function () {
      res.send(status)
    }, 500)
  })
})
router.get('/upload', function (req, res) {
  uploader.get(req, function (status, filename, original_filename, identifier) {
    if (ACCESS_CONTROLL_ALLOW_ORIGIN) {
      res.header('Access-Control-Allow-Origin', '*')
    }
    res.status(status == 'found' ? 200 : 204).send()
  })
})


router.post('/loadfiles', function (req, res) {
  db.media.getAll(req.body.data).then(function (media) {
    let mediasfould = media.map(media => new Object({
      filename: media.filename,
      media_type: media.media_type,
      folder: media.package.package_folder
    }))
    res.status(200).json({
      errorflag: false,
      images: mediasfould
    })
  }).catch(function (err) {
    console.log(err)
    // your error handling code here
  })
})

router.get('/serverstatic', auth.required, function (req, res) {
  res.type(req.query.media).sendFile('projects_dist/' + req.query.fold + '/src/assets/media/' + req.query.fname, {
    root: '../'
  })
})
// findCreateFind
router.get('/file/:package/:filename', function (req, res) {
  db.media.getOne(req.params.filename, req.params.package).then(function (media) {
    res.type(media.media_type).sendFile('projects_dist/' + media.package.package_folder + '/src/assets/media/' + media.filename, {
      root: '../'
    })
  }).catch(function (err) {
    console.log(err)
    res.type('image/jpeg').sendFile('projects_dist/pilotdist/src/assets/media/180x180.jpg', {
      root: '../'
    })
    // your error handling code here
  })
})
router.get('/thumb/:package/:filename', function (req, res) {
  db.media.getThumb(req.params.filename, req.params.package).then(function (media) {
    res.type(media.media_type).sendFile('projects_dist/pilotdist/src/assets/media/180x180.png', {
      root: '../'
    })
  }).catch(function (err) {
    console.log(err)
    // your error handling code here
  })
})
router.post('/cleanupchunk', function (req, res) {
  fs.readdir('../tmp/', (err, allfiles) => {
    if (err) throw err

    for (const onefile of allfiles) {
      fs.unlink(path.join('../tmp/', onefile), err => {
        if (err) throw err
      })
    }
  })
})

router.post('/gallery/allpackages', function (req, res) {
  db.packages.findAll({
    attributes: ['package_name', 'id'],
    where: {
      id: req.body.pid
    }
  }).then(function (entries) {
    let packages = entries.map(entries => new Object({
      pid: entries.id,
      packagename: entries.package_name
    }))
    res.status(200).json({
      errorflag: false,
      pack: packages
    })
  }).catch(err => {
    console.log(pe.render(new Error('Gallery Find All Packages error')))
    res.status(500)
    console.log(pe.render(err))
    res.json({
      errorflag: true,
      message: err
    })
    res.end()
  })
})
router.get('/gallery/getmedia', function (req, res) {
  packageid = req.query.pid
  db.media.findAndCountAll({
    where: {
      package_id: packageid
    },
    limit: parseInt(req.query.limit),
    offset: parseInt(req.query.page)
  }).then((media) => {
    let entries = media.rows
    let mediafiles = entries.map(entries => new Object({
      id: entries.media_id,
      filename: entries.filename,
      media_type: entries.media_type
    }))
    res.status(200).json({
      errorflag: false,
      media: mediafiles,
      count: media.count
    })
  }).catch(err => {
    console.log(pe.render(new Error('Gallery Find All Packages error')))
    res.status(500)
    console.log(pe.render(err))
    res.json({
      errorflag: true,
      message: err
    })
    res.end()
  })
})
router.get('/gallery/filteredmedia', function (req, res) {
  packageid = req.query.pid
  typefile = req.query.typefile
  db.media.findAndCountAll({
    where: {
      $and: [{
        package_id: packageid
      }, {
        media_type: {
          $like: '%' + typefile + '%'
        }
      }]
    },
    limit: parseInt(req.query.limit),
    offset: parseInt(req.query.page)
  }).then((media) => {
    let entries = media.rows
    let mediafiles = entries.map(entries => new Object({
      id: entries.media_id,
      filename: entries.filename,
      media_type: entries.media_type
    }))
    res.status(200).json({
      errorflag: false,
      media: mediafiles,
      count: media.count
    })
  }).catch(err => {
    console.log(pe.render(new Error('Gallery Find All Packages error')))
    res.status(500)
    console.log(pe.render(err))
    res.json({
      errorflag: true,
      message: err
    })
  })
})
router.get('/gallery/getfilebyname', function (req, res) {
  packageid = req.query.pid
  filename = req.query.filename
  db.media.findAndCountAll({
    where: {
      $and: [{
        package_id: packageid
      }, {
        filename: {
          $like: '%' + filename + '%'
        },
        media_type: {
          $like: '%' + req.query.typefile + '%'
        }
      }]
    },
    limit: parseInt(req.query.limit),
    offset: parseInt(req.query.page)
  }).then((media) => {
    let entries = media.rows
    let mediafiles = entries.map(entries => new Object({
      id: entries.media_id,
      filename: entries.filename,
      media_type: entries.media_type
    }))
    res.status(200).json({
      errorflag: false,
      media: mediafiles,
      count: media.count
    })
  }).catch(err => {
    console.log(pe.render(new Error('Gallery Find All Packages error')))
    res.status(500)
    console.log(pe.render(err))
    res.json({
      errorflag: true,
      message: err
    })
    res.end()
  })
})

router.post('/gallery/getpackage', function (req, res) {
  db.packages.findAll({
    limit: 1,
    attributes: ['package_folder', 'id'],
    where: {
      id: req.body.package
    }
  }).then(function (entries) {

  })
})

module.exports = router
