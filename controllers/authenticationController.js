const passport = require('passport')
const jwt = require('jsonwebtoken')
const db = require('../models')

const authenticationController = {
  signIn (req, res, next) {
    const {username, password} = req.body
    if (!username) {
      return res.status(400).json({
        error: true,
        message: 'Username is required'
      })
    }
    if (!password) {
      return res.status(400).json({
        error: true,
        message: 'password is required'
      })
    }
    return passport.authenticate('local', async (err, passportUser) => {
      if (err) return next(err)

      if (!passportUser) {
        return res.status(401).json({error: true,
          message: 'user not found' })
      }
      // Only for session auth
      // req.login(passportUser, (err) => {
      //   if (err) {
      //     console.log('sign err', err)
      //     return next(err)
      //   }
      //   return res.status(200).json({error: false, message: 'User is now authenticated', user: passportUser})
      // })

      // Only for token auth
      let token = passportUser.generateJWT()
      let user = passportUser.toMinimumFields()
      res.cookie('jwt', token, {
        maxAge: 1000 * 60 * 60 * 24, // 24h
        httpOnly: true
      })
      return res.status(200).json({error: false, message: 'User is now authenticated', user, token})
    })(req, res, next)
  },
  signOut (req, res) {
    // FOR SESSION
    // req.logout()
    // res.clearCookie('digi_sid')
    // req.session = null // deletes session stored in mongo store

    // for jwt
    res.clearCookie('jwt')
    return res.status(200).json({error: false, message: 'Successful logout'})
  },
  async getCurrentUser (req, res) {
    // jtw only
    if (req && req.cookies) {
      try {
        if (!req.cookies['jwt']) {
          return res.status(418).json({error: false, message: 'No cookie supplied', user: null})
        }
        let decoded = await jwt.verify(req.cookies['jwt'], process.env.JWT_SECRET)
        console.log('cookie decoded', decoded)
        let user = await db.users.findByPk(decoded.data.id)
        if (!user) return res.status(200).json({ error: true, message: 'No such user exists.' })
        return res.status(200).json({error: false, message: 'You are authenticated.', user: user.toMinimumFields()})
      } catch (err) {
        // err
        console.log('err expired?')
        res.clearCookie('jwt')
        return res.status(401).json({ error: true, message: 'Token expired,please login.' })
      }
    }

    // only session
    // let user = req.user
    // if (!user) {
    //   return res.status(401).json({ error: true, message: 'User not authenticated' })
    // }
    // const minimunUser = req.user.toMinimumFields()
    // return res.status(200).json({error: false, user: minimunUser})
  }
}
module.exports = authenticationController
