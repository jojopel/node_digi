var pe = require('../helpers/errorConfig')
const db = require('../models')
var auth = require('../controllers/auth.js')
const middlewares = require('../middlewares')
var Mylogger = require('../server/main.js')
// var sequelize = require('../server/main.js')
var router = require('express').Router()
var fs = require('fs')
/**
 * Export Module is 3 phase procedure
 * 1st phase: copy media to destinarion package and create media records for this package (@function copyMedia)
 * 2nd phase: create new module to destination package from source module (@function copyModule)
 * 3rd phase: copy pages from source module to destination module (@function createModulePages)
 * /

 /** return Promise
 * 1) call store Procedure exportModuleProcedure (finds which media used in this module)
 * 2) Copy those media to destination Package (@function createNewMedia)
 * 3) Create the new db records for previous media(@function createCopyMediaToDB)
 * 4) resolve Promise
 * @param {ID of the user that made the export request} userID
 * @param {ID of the source export module} moduleID
 * @param {ID of the source package that module belongs} packageSourceID
 * @param {ID of the destination package that module will exported to} packageDestinationID
 * @param {Folder Name} packageSourceFolder
 * @param {Folder Name} packageDestinationFolder
 */
const copyMedia = (userID, moduleID, packageSourceID, packageDestinationID, packageSourceFolder, packageDestinationFolder) => {
  return new Promise((resolve, reject) => {
    let mediaModule = ('\'' + moduleID + '\',').slice(0, -1)
    let processingAssets = []
    db.sequelize.query('CALL exportModuleProcedure(:pid, :moduleids)', {
      replacements: {
        pid: packageSourceID,
        moduleids: mediaModule
      },
      type: db.sequelize.QueryTypes.RAW
    }).then((assets) => {
      return Promise.all(
        assets.map((asset) => {
          processingAssets.push(asset)
          return createNewMedia(asset.medianame, packageSourceID, packageSourceFolder, packageDestinationFolder)
        })
      )
    }).then(() => {
      return Promise.all(
        processingAssets.map((asset) => {
          return createCopyMediaToDB(userID, asset.medianame, packageSourceID, packageDestinationID)
        })
      )
    }).then((response) => {
      resolve('Copy was successful')
    }).catch((error) => {
      reject(error)
    })
  })
}
/**
 * copy media to destination package
 * @param {*} mediaName
 * @param {*} packageID
 * @param {*} packageSourceFolder
 * @param {*} packageDestinationFolder
 */
const createNewMedia = (mediaName, packageID, packageSourceFolder, packageDestinationFolder) => {
  return new Promise((resolve, reject) => {
    db.media.findOne({where: {$and: [{filename: mediaName}, {package_id: packageID}]}})
      .then((media) => {
        return copyNewMedia('../projects_dist/' + packageSourceFolder + '/src/assets/media/' + media.filename, '../projects_dist/' + packageDestinationFolder + '/src/assets/media/' + media.filename)
      }).then((response) => {
        resolve('Media Filename: ' + response + ' copied successfully!!')
      })
      .catch((error) => {
        reject(error)
      })
  })
}
/**
 * copy media promise function
 * @param {*} source
 * @param {*} destination
 */
const copyNewMedia = (source, destination) => {
  return new Promise((resolve, reject) => {
    let readStream = fs.createReadStream(source)
    let writeStream = fs.createWriteStream(destination)
    // Error in streams
    readStream.on('error', (error) => {
      reject(error)
    })
    writeStream.on('error', (error) => {
      reject(error)
    })
    // Finish Streams
    writeStream.on('finish', () => {
      resolve(destination)
    })
    readStream.pipe(writeStream)
    // fs.copyFile(source, destination, (err) => {
    //   if (err) {
    //     reject(err)
    //   } else {
    //     resolve(destination)
    //   }
    // })
  })
}
/**
 * create new media db records
 * @param {*} userID
 * @param {*} mediaFilename
 * @param {*} packageSourceID
 * @param {*} packageDestinationID
 */
const createCopyMediaToDB = (userID, mediaFilename, packageSourceID, packageDestinationID) => {
  return new Promise((resolve, reject) => {
    db.media.findOne({where: {$and: [{
      package_id: packageSourceID
    }, {
      filename: mediaFilename
    }]}}).then((initialMedia) => {
      return db.media.findOrCreate({
        where: {
          $and: [{
            package_id: packageDestinationID
          }, {
            filename: mediaFilename
          }]
        },
        defaults: {
          package_id: packageDestinationID,
          filename: mediaFilename,
          media_type: initialMedia.media_type,
          created_by: userID,
          modified_by: userID
        }
      })
    }).then((response) => {
      resolve(response)
    }).catch((error) => {
      reject(error)
    })
  })
}
/**
 * find source module instance
 * create new module in destination package (new module name sourceModuleName + _exported)
 * reOrder destination package modules after the addition of one new module (@function reOrderModules)
 * @param {*} userID
 * @param {*} moduleID
 * @param {*} packageSourceID
 * @param {*} packageDestinationID
 */
const copyModule = (userID, moduleID, packageSourceID, packageDestinationID) => {
  let exportedModuleID = null
  return new Promise((resolve, reject) => {
    db.modules.findOne({where: {module_id: moduleID}})
      .then((initialModule) => {
        let exportName = initialModule.module_name + '_exported'
        return db.modules.findOrCreate({
          where: {
            $and: [{
              package_id: packageSourceID
            },
            {
              module_name: exportName
            }
            ]
          },
          defaults: {
            package_id: packageDestinationID,
            module_name: exportName,
            module_order: initialModule.module_order,
            module_status: 1,
            created_by: userID,
            modified_by: userID
          }
        })
      }).then((exportedModule) => {
        exportedModuleID = exportedModule[0].dataValues.module_id
        return reOrderModules(packageDestinationID)
      }).then(() => {
        return createModulePages(userID, moduleID, exportedModuleID)
      }).then((response) => {
        resolve(response)
      }).catch((error) => {
        reject(error)
      })
  })
}
/**
 * reOrder package modules based
 * get array of all modules for destination Package order by module_order
 * reorder based on array index
 * @param {*} packageDestinationID
 */
const reOrderModules = (packageDestinationID) => {
  return new Promise((resolve, reject) => {
    db.modules.findAll({where: {package_id: packageDestinationID}, order: [['module_order', 'ASC']]})
      .then((allModules) => {
        let promiseMap = new Map()
        // create sequentially await promises
        allModules.forEach((updateModule, index) => {
          promiseMap.set([updateModule, (index + 1)], updateModuleOrder)
        })
        return runPromiseMap(promiseMap)
      }).then((response) => {
        resolve(response)
      }).catch((error) => {
        reject(error)
      })
  })
}
/**
 * using new Map to create the promiseMap array
 * resolve each promise sequentially
 * @param {array with args (module order) + promise function update} promiseMap
 */
const runPromiseMap = (promiseMap) => {
  return new Promise((resolve, reject) => {
    try {
      Array.from(promiseMap.keys()).reduce((promise, nextPromise) => {
        return promise.then((result) => {
          promiseMap.get(nextPromise).apply(this, nextPromise)
        })
      }, Promise.resolve([]))
      resolve('Re Order Module was Successful')
    } catch (error) {
      reject(error)
    }
  })
}
// update module order promise
const updateModuleOrder = (moduleInstance, moduleOrder) => {
  return moduleInstance.update({module_order: moduleOrder})
}
/**
 * copy pages from source module to destination module
 * keep:
 * page_name, page_json, page_order
 * remove: (initialize to null)
 * digi assign data
 * @param {*} userID
 * @param {*} moduleSourceID
 * @param {*} moduleDestinationID
 */
const createModulePages = (userID, moduleSourceID, moduleDestinationID) => {
  return new Promise((resolve, reject) => {
    db.pages.findAll({where: {module_id: moduleSourceID}})
      .then((initialPages) => {
        return Promise.all(
          initialPages.map((initialPages) => {
            return db.pages.create({
              page_name: initialPages.dataValues.page_name,
              page_json: initialPages.dataValues.page_json,
              page_order: initialPages.dataValues.page_order,
              locked: 0,
              locked_by: 0,
              page_url: null,
              digi_activity_id: null,
              digi_activity_name: null,
              digi_module_id: null,
              digi_module_name: null,
              digi_skilltype_id: null,
              digi_skilltype_name: null,
              digi_keywords: null,
              digi_category: null,
              digi_weight: null,
              digi_eleclimitation: 0,
              created_by: userID,
              modified_by: userID,
              module_id: moduleDestinationID
            })
          })
        )
      }).then((response) => {
        resolve(response)
      }).catch((error) => {
        reject(error)
      })
  })
}
router.post('/', auth.required, middlewares.isAdmin, (req, res) => {
  if (req.err) {
    console.log(pe.render(new Error('Request Error')))
    console.log(pe.render(req.err))
    Mylogger.mylogger.SequelizeLogger.error(req.err.message)
    res.status(500)
    res.json({
      errorflag: true,
      message: req.err
    })
    res.end()
  } else {
    if (req.body) {
      copyMedia(req.user.user_id, req.body.moduleID, req.body.packageSourceID, req.body.packageDestinationID, req.body.packageSourceFolder, req.body.packageDestinationFolder)
        .then((response) => {
          return copyModule(req.user.user_id, req.body.moduleID, req.body.packageSourceID, req.body.packageDestinationID)
        }).then((response) => {
          res.status(200)
          res.json({
            errorflag: false,
            message: 'Module Export Finished Successfully!!'
          })
          res.end()
        }).catch((error) => {
          console.log(pe.render(new Error(error)))
          Mylogger.mylogger.SequelizeLogger.error(error)
          res.status(500)
          res.json({
            errorflag: true,
            message: error
          })
          res.end()
        })
    } else {
      console.log(pe.render(new Error('Request Error')))
      Mylogger.mylogger.SequelizeLogger.error('Data is not Available')
      res.status(500)
      res.json({
        errorflag: true,
        message: 'Data is not Available'
      })
      res.end()
    }
  }
})
module.exports = router
