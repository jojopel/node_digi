const serviceContainer = require('../services')
const db = require('../models')
var folderfunc = require('../helpers/folderFunctions')
// var Mylogger = require('../server/main')
var pe = require('../helpers/errorConfig')
var _ = require('lodash')

const packageController = {
  getRecent (req, res) {
    return res.status(200).json({
      error: false,
      message: 'START'
    })
  },
  async getPackage (req, res) {
    try {
      let pckg = await db.packages.findOne({
        where: {package_name: req.params.package_name},
        include: ['packageuser', 'PackageCreator', 'PackageCategory', 'packagesection',
          {
            model: db.pilotversions,
            attributes: ['pilot_id', 'pilot_name'],
            distinct: true
          }]
      })
      if (pckg) {
        // let packageusers = await pckg.getUsers()
        return res.status(200).json({
          error: false,
          message: 'Found package',
          package: pckg
          // users: packageusers
        })
      }
      return res.status(200).json({
        error: true,
        message: 'No such package exists'
      })
    } catch (err) {
      throw err
    }
  },
  async getPackageUnassignedUsers (req, res) {
    try {
      let pckg = await db.packages.findOne({
        where: {package_name: req.params.package_name}
      })
      let asd = await pckg.getPackageuser()
      let ids = asd.map(user => user.user_id)
      let un = await db.users.findAll({
        attributes: ['user_id', 'firstname', 'lastname', 'role_id'],
        where: {
          user_id: {
            [db.Sequelize.Op.notIn]: ids
          },
          role_id: {
            [db.Sequelize.Op.notIn]: [1, 5]
          },
          user_status: {
            [db.Sequelize.Op.ne]: 0
          }
        }
      })
      return res.status(200).json({
        error: false,
        message: 'Found package assisgnes',
        users: un
      })
    } catch (err) {
      return res.status(500).json({
        error: true,
        message: err
      })
    }
  },
  async assignUserToPackage (req, res) {
    let userId = req.body.user_id
    if (userId) {
      try {
        let pckg = await db.packages.findOne({
          where: {package_name: req.params.package_name}
        })
        let usertobeassigned = await db.users.findByPk(userId)

        await pckg.addPackageuser(usertobeassigned)

        let data = {
          user_id: usertobeassigned.user_id,
          firstname: usertobeassigned.firstname,
          lastname: usertobeassigned.lastname
        }
        return res.status(200).json({
          error: false,
          message: 'Successfull Assignment',
          user: data
        })
      } catch (err) {
        console.log(err)
        return res.status(500).json({
          error: true,
          message: err
        })
      }
    }
  },
  async unAssignUserFromPackage (req, res) {
    let userId = req.body.user_id
    if (!userId) {
      return res.status(400).json({
        error: true,
        message: 'No user provided'
      })
    }
    let pckg = await db.packages.findOne({
      where: {package_name: req.params.package_name}
    })
    if (!pckg) {
      return res.status(400).json({
        error: true,
        message: 'No such package'
      })
    }
    let usertobeunassigned = await db.users.findByPk(userId)
    if (usertobeunassigned) {
      try {
        let result = await db.packagestousers.destroy({
          where: {
            package_id: pckg.id,
            user_id: usertobeunassigned.user_id
          }
        })
        if (result) {
          return res.status(200).json({
            error: false,
            message: 'Successfull Unassignment'
          })
        }
      } catch (err) {
        console.log(err)
        return res.status(500).json({
          error: true,
          message: err
        })
      }
    }
    return res.status(500).json({
      error: true,
      message: "Users doesn't exist."
    })
  },
  async getPackageModules (req, res) {
    try {
      let pckg = await db.packages.findOne({
        where: {package_name: req.params.package_name}
      })
      if (!pckg) {
        return res.status(400).json({
          error: true,
          message: 'No such package'
        })
      }

      let packageModules = await db.modules.findAll({
        include: [{
          as: 'module_createdby',
          model: db.users,
          attributes: ['username']
        },
        {
          as: 'module_modifiedby',
          model: db.users,
          attributes: ['username']
        }
        ],
        where: {
          package_id: pckg.id
        },
        attributes: ['package_id', 'module_id', 'module_name', 'module_order', 'updatedAt', 'createdAt'],
        order: ['module_order'],
        limit: req.body.paginationlimit,
        offset: req.body.paginationoffset
      })

      console.log(packageModules)
      if (packageModules.length > 0) {
        return res.status(200).json({
          error: false,
          message: 'Found Modules.',
          modules: packageModules
        })
      }
      return res.status(200).json({
        error: false,
        message: 'No Modules exist yet.'
      })
    } catch (err) {
      console.log(err)
      return res.status(500).json({
        error: true,
        message: err
      })
    }
  },
  async getPackageModulesCount (req, res) {
    try {
      let pckg = await db.packages.findOne({
        where: {package_name: req.params.package_name}
      })

      if (pckg) {
        let count = await db.modules.count({
          where: {
            package_id: pckg.id
          }
        })
        return res.status(200).json({
          errorflag: false,
          count: count
        })
      }
    } catch (err) {
      console.log(err)
      return res.status(500).json({
        error: true,
        message: err
      })
    }
  },
  async getPackages (req, res) {
    try {
      const packages = await serviceContainer.packageService.getPackages(req.user)
      return res.status(200).json({
        error: false,
        message: 'Found packages',
        packages: packages
      })
    } catch (error) {
      return res.status(500).json({
        error: true,
        message: error
      })
    }
  },
  clearAssets (req, res) {
    let modulesids = ''
    req.body.data.moduleids.forEach((id) => {
      modulesids = modulesids + '\'' + id + '\','
    })
    req.body.data.moduleids = modulesids.slice(0, -1)
    db.sequelize.query('CALL clearassetsbeta(:pid, :moduleids)', {
      replacements: {
        pid: req.body.data.pid,
        moduleids: req.body.data.moduleids
      },
      type: db.sequelize.QueryTypes.RAW
    }).then((assets) => {
      return res.status(200).json({
        errorflag: false,
        message: assets
      })
    }).catch((error) => {
      console.error(error)
      return res.status(500).json({
        errorflag: true,
        message: error
      })
    })
  },
  createPackage (req, res) {
    req.body.data.package_folder = req.body.data.package_name.toString().toLowerCase().trim().replace(/\s+/g, '_')
    var newpackage = db.packages.build(req.body.data)
    newpackage.created_by = req.user.user_id
    newpackage.modified_by = req.user.user_id
    newpackage.validate().then(() => {
      folderfunc.copypackage(req.body.data.package_folder, '/pilotdist', function (error, packagedir) {
        if (error) {
          console.log(pe.render(new Error('Copy Package Error')))
          console.log(pe.render(error))
          // Mylogger.mylogger.SequelizeLogger.error(error.message)
          return res.status(500).json({
            errorflag: true,
            message: packagedir
          })
        } else {
          newpackage.save().then(() => {
            return res.status(200).json({
              errorflag: false,
              message: 'Package Saved Succesfully'
            })
          }).catch((err) => {
            console.log(pe.render(new Error('Save Package')))
            console.log(pe.render(err))
            // Mylogger.mylogger.SequelizeLogger.error(err.message)
            return res.status(500).json({
              errorflag: true,
              message: err
            })
          })
        }
      })
    }).catch((err) => {
      // Mylogger.mylogger.SequelizeLogger.error(err.message)
      console.log(pe.render(err))
      return res.status(201).json({
        errorflag: true,
        message: err
      })
    })
  },
  gridView (req, res) {
    if (req.body.package_name === undefined) {
      req.body.package_name = '%'
    }
    if (req.body.isbn === undefined) {
      req.body.isbn = '%'
    }
    if (req.body.format === undefined) {
      req.body.format = '%'
    }
    if (req.body.category === undefined) {
      req.body.category = '^[0-9]+'
    } else {
      req.body.category = '^[' + req.body.category + ']+'
    }
    if (req.body.assigneduser === undefined) {
      if (req.user.role_id !== 1) {
        db.packagestousers.findAll({
          attributes: ['package_id'],
          where: {
            user_id: req.user.user_id
          }
        }).then((items) => {
          // if (items.length) {
          //   return res.status(201).json({
          //     errorflag: true,
          //     message: 'No packages assigned yet!'
          //   })
          // }
          let idarray = _.map(items, function (item) {
            return item.dataValues.package_id
          })
          db.packages.findAndCountAll({
            distinct: true,
            include: [{
              model: db.packagecategories,
              as: 'PackageCategory',
              attributes: ['category_name'],
              distinct: true
            }, {
              model: db.users,
              as: 'packageuser',
              attributes: ['user_id', 'username'],
              required: true,
              distinct: true
            }, {
              model: db.users,
              as: 'PackageModifiedBy',
              attributes: ['username'],
              required: true,
              distinct: true
            }, {
              model: db.modules,
              attributes: ['module_id', 'module_name']
            },
            {
              model: db.pilotversions,
              attributes: ['pilot_id', 'pilot_name'],
              distinct: true
            }],
            where: {
              $and: [{
                package_name: {
                  $like: '%' + req.body.package_name + '%'
                }
              }, {
                isbn: {
                  $like: '%' + req.body.isbn + '%'
                }
              }, {
                format: {
                  $like: '%' + req.body.format + '%'
                }
              }, {
                package_category: {
                  $regexp: req.body.category
                }
              }, {
                id: {
                  $in: idarray
                }
              }]
            },
            order: [[db.Sequelize.col('updatedAt'), 'DESC'], [db.modules, 'module_order', 'ASC']],
            limit: req.body.paginationlimit,
            offset: req.body.paginationoffset
          }).then(packages => {
            if (packages.length < 1) {
              res.status(201)
              res.json({
                errorflag: true,
                message: 'Packages not found'
              })
              res.end()
            } else {
              packages.count = packages.count
              res.status(200)
              res.json({
                errorflag: false,
                packages: packages
              })
              res.end()
            }
          }).catch(err => {
            console.log(pe.render(new Error('Find And Count All Packages')))
            res.status(500)
            // // Mylogger.mylogger.SequelizeLogger.error(err.message)
            console.log(pe.render(err))
            res.json({
              errorflag: true,
              message: err
            })
            res.end()
          })
        }).catch((err) => {
          // // Mylogger.mylogger.SequelizeLogger.error(err.message)
          console.log(pe.render(err))
        })
      } else {
        console.log('else', req.body)
        db.packages.findAndCountAll({
          distinct: true,
          include: [
            {
              model: db.packagecategories,
              as: 'PackageCategory',
              attributes: ['category_name'],
              distinct: true
            },
            {
              model: db.users,
              as: 'packageuser',
              attributes: ['user_id', 'username'],
              distinct: true
            },
            {
              model: db.users,
              // as: 'packagemodified',
              as: 'PackageModifiedBy',
              attributes: ['username'],
              required: true,
              distinct: true
            }, {
              model: db.modules,
              attributes: ['module_id', 'module_name', 'module_order']
            },
            {
              model: db.pilotversions,
              attributes: ['pilot_id', 'pilot_name'],
              distinct: true
            }],
          where: {
            $and: [{
              package_name: {
                $like: '%' + req.body.package_name + '%'
              }
            }, {
              isbn: {
                $like: '%' + req.body.isbn + '%'
              }
            }, {
              format: {
                $like: '%' + req.body.format + '%'
              }
            }, {
              package_category: {
                $regexp: req.body.category
              }
            }]
          },
          order: [[db.Sequelize.col('updatedAt'), 'DESC'], [db.modules, 'module_order', 'ASC']],
          limit: req.body.paginationlimit,
          offset: req.body.paginationoffset
        }).then(packages => {
          if (packages.length < 1) {
            res.status(201)
            res.json({
              errorflag: true,
              message: 'Packages not found'
            })
            res.end()
          } else {
            packages.count = packages.count
            res.status(200)
            res.json({
              errorflag: false,
              packages: packages
            })
            res.end()
          }
        }).catch(err => {
          console.log(pe.render(new Error('Find And Count All Packages')))
          res.status(500)
          // Mylogger.mylogger.SequelizeLogger.error(err.message)
          console.log(pe.render(err))
          res.json({
            errorflag: true,
            message: err
          })
          res.end()
        })
      }
    }
  },
  getAssigned (req, res) {
    db.packagestousers.destroy({
      where: {
        package_id: req.body.packageid
      }
    }).then(() => {
      let parseddata = JSON.parse(req.body.jsondata)
      var findorcreatearray = []
      for (var i = 0; i < parseddata.length; i++) {
        var findorcreate = db.packagestousers.create({ user_id: parseddata[i].user_id, package_id: parseddata[i].package_id })
        findorcreatearray.push(findorcreate)
      }
      var promises = Promise.all(findorcreatearray)
      promises.then(() => {
        res.status(200)
        res.json({
          errorflag: false,
          message: 'All Users Assigned to Package'
        })
        res.end()
      }).catch((err) => {
        console.log(pe.render(new Error('Find Or Create Packages')))
        console.log(pe.render(err))
        // Mylogger.mylogger.SequelizeLogger.error(err.message)
        if (Object.keys(err).length === 0) {
          err = err.message
        }
        res.status(201)
        res.json({
          errorflag: true,
          message: err
        })
        res.end()
      })
    }).catch((err) => {
      console.log(pe.render(new Error('Delete Assigned Users Packages')))
      console.log(pe.render(err))
      // Mylogger.mylogger.SequelizeLogger.error(err.message)
      res.status(500)
      res.json({
        errorflag: true,
        message: err
      })
      res.end()
    })
  },
  deletePackageUsers (req, res) {
    db.packagestousers.destroy({
      where: {
        package_id: req.params.id
      }
    }).then(() => {
      return res.status(200).json({
        errorflag: false,
        message: 'Delete packages to users associations was successfull'
      })
    }).catch((err) => {
      console.log(pe.render(new Error('Destroy Packages')))
      console.log(pe.render(err))
      // Mylogger.mylogger.SequelizeLogger.error(err.message)
      return res.status(500).json({
        errorflag: true,
        message: err
      })
    })
  },
  updatePackage (req, res) {
    db.packages.findOne({
      where: {
        id: req.body.data.package_id
      }
    }).then((updatepackage) => {
      updatepackage.update({
        package_name: req.body.data.package_name,
        package_category: req.body.data.package_category,
        image: req.body.data.image,
        isbn: req.body.data.isbn,
        format: req.body.data.format,
        version: req.body.data.version,
        iso: req.body.data.iso,
        download_videos: req.body.data.download_videos,
        assigned: req.body.data.assigned,
        package_status: req.body.data.package_status,
        digi_package_id: req.body.data.digi_package_id,
        modified_by: req.user.user_id,
        modified: null
      }).then((response) => {
        return res.status(200).json({
          errorflag: false,
          message: 'Package Updated Succesfully'
        })
      }).catch((err) => {
        if (Object.keys(err).length === 0) {
          err = err.message
        }
        console.log(pe.render(err))
        // Mylogger.mylogger.SequelizeLogger.error(err.message)
        return res.status(201).json({
          errorflag: true,
          message: err
        })
      })
    }).catch((err) => {
      console.log(pe.render(new Error('Find One Package')))
      console.log(pe.render(err))
      // Mylogger.mylogger.SequelizeLogger.error(err.message)
      return res.status(500).json({
        errorflag: true,
        message: err
      })
    })
  },
  findPackage (req, res) {
    db.packages.findOne({
      where: {
        id: req.params.id
      }
      // include: [{
      //   model: db.packagecategories,
      //   as: 'PackageCategory',
      //   attributes: ['category_name']
      // }]
    }).then((findpackage) => {
      return res.status(200).json({
        errorflag: false,
        package: findpackage
      })
    }).catch(err => {
      console.log(pe.render(new Error('Find One Package')))
      console.log(pe.render(err))
      // Mylogger.mylogger.SequelizeLogger.error(err.message)
      return res.status(500).json({
        errorflag: true,
        message: err
      })
    })
  },
  findAllPackages (req, res) {
    if (req.user.role_id !== 1) {
      db.packagestousers.findAll({
        attributes: ['package_id'],
        where: {
          user_id: req.user.user_id
        }
      }).then((items) => {
        let idarray = _.map(items, function (item) {
          return item.dataValues.package_id
        })
        db.packages.findAll({
          attributes: ['id', 'package_name'],
          where: {
            id: {
              $in: idarray
            }
          }
        }).then((packages) => {
          return res.status(200).json({
            errorflag: false,
            packages: packages
          })
        }).catch(err => {
          console.log(pe.render(new Error('Find All Packages')))
          console.log(pe.render(err))
          // Mylogger.mylogger.SequelizeLogger.error(err.message)
          return res.status(500).json({
            errorflag: true,
            message: err
          })
        })
      }).catch((err) => {
        // Mylogger.mylogger.SequelizeLogger.error(err.message)
        console.log(pe.render(err))
      })
    } else {
      db.packages.findAll({
        attributes: ['id', 'package_name']
      }).then((packages) => {
        return res.status(200).json({
          errorflag: false,
          packages: packages
        })
      }).catch(err => {
        console.log(pe.render(new Error('Find All Packages')))
        console.log(pe.render(err))
        // Mylogger.mylogger.SequelizeLogger.error(err.message)
        return res.status(500).json({
          errorflag: true,
          message: err
        })
      })
    }
  },
  deletePackage (req, res, next) {
    db.packages.findOne({
      where: {
        id: req.body.id
      }
    }).then((findpackage) => {
      if (!findpackage) {
        console.log(pe.render('Package not found'))
        res.status(201)
        res.json({
          errorflag: true,
          message: 'Package not found'
        })
        res.end()
      } else {
        db.modules.findAll({
          where: {
            package_id: req.body.id
          }
        }).then((modules) => {
          if (modules.length > 0) {
            return res.status(201).json({
              errorflag: true,
              message: 'Package have modules assigned to it. Cannot be deleted!!!'
            })
          } else {
            let promisearray = []
            promisearray.push(db.packages.destroy({
              where: {
                id: req.body.id
              }
            }))
            promisearray.push(db.packagestousers.destroy({
              where: {
                package_id: req.body.id
              }
            }))
            promisearray.push(db.sectionstopackages.destroy({
              where: {
                package_id: req.body.id
              }
            }))
            promisearray.push(db.media.destroy({
              where: {
                package_id: req.body.id
              }
            }))
            Promise.all(promisearray).then(() => {
              folderfunc.deletefolder(findpackage.package_folder, function (error, message) {
                if (error) {
                  console.log(pe.render(new Error('Copy Package Error')))
                  console.log(pe.render(error))
                  // Mylogger.mylogger.SequelizeLogger.error(error.message)
                  return res.status(500).json({
                    errorflag: true,
                    message: message
                  })
                } else {
                  return res.status(200).json({
                    errorflag: false,
                    message: 'Package Deleted Succesfully'
                  })
                }
              })
            }).catch((error) => {
              console.log(pe.render(new Error('Find Delete Package Assigments')))
              res.status(500)
              console.log(pe.render(error))
              // Mylogger.mylogger.SequelizeLogger.error(error.message)
              res.json({
                errorflag: true,
                message: error
              })
              res.end()
            })
          }
        }).catch(error => {
          console.log(pe.render(new Error('Find Delete Package Module')))
          res.status(500)
          console.log(pe.render(error))
          // Mylogger.mylogger.SequelizeLogger.error(error.message)
          res.json({
            errorflag: true,
            message: error
          })
          res.end()
        })
      }
    }).catch(error => {
      console.log(pe.render(new Error('Find Delete Package Module')))
      res.status(500)
      console.log(pe.render(error))
      // Mylogger.mylogger.SequelizeLogger.error(error.message)
      res.json({
        errorflag: true,
        message: error
      })
      res.end()
    })
  },
  getPackageCategories (req, res, next) {
    db.packagecategories.findAll({
      attributes: ['category_id', 'category_name']
    }).then((categories) => {
      return res.status(200).json({
        errorflag: false,
        categories: categories
      })
    }).catch(err => {
      console.log(pe.render(new Error('Find All Categories')))
      console.log(pe.render(err))
      // Mylogger.mylogger.SequelizeLogger.error(err.message)
      return res.status(500).json({
        errorflag: true,
        message: err
      })
    })
  },
  getSections (req, res) {
    db.packages.findAll({
      include: [{
        model: db.sections,
        as: 'packagesection',
        attributes: ['section_id', 'section_name', 'section_category', 'created_by', 'updatedAt'],
        distinct: true,
        include: [{ model: db.users, as: 'creator_role', attributes: ['role_id'] }]
      }
      ],
      where: {
        id: req.body.data
      }
    }).then((sections) => {
      return res.status(200).json({
        errorflag: false,
        sections: sections
      })
    }).catch(err => {
      console.log(pe.render(new Error('Find All Packages')))
      console.log(pe.render(err))
      // Mylogger.mylogger.SequelizeLogger.error(err.message)
      return res.status(500).json({
        errorflag: true,
        message: err
      })
    })
  },
  getAllPackages (req, res) {
    db.packages.findAll()
      .then((packages) => {
        return res.status(200).json({
          errorflag: false,
          packages: packages
        })
      }).catch((err) => {
        console.log(pe.render(new Error('Find All Packages')))
        console.log(pe.render(err))
        // Mylogger.mylogger.SequelizeLogger.error(err.message)
        return res.status(500).json({
          errorflag: true,
          message: err
        })
      })
  }
}
module.exports = packageController
