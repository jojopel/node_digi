// import 'dotenv/config'

// require('dotenv').config()
const express = require('express')
const config = require('../config')
const env = process.env.NODE_ENV ? process.env.NODE_ENV : 'development'
const initApp = require('../loaders')

const {logger} = require('../loaders/logger')

async function startServer () {
  const server = express()
  // server.use(express.static('../projects_dist'))

  try {
    await initApp(server)
  } catch (err) {
    console.log(err)
  }

  server.listen(config[env].port, (err) => {
    if (err) {
      console.log(err)
      return
    }
    console.log(`The server is running at http://localhost:${config[env].port}/`)
    console.log(`The environment is,
    ${env} : `, config[env])
  })
}

startServer()

module.exports = {
  mylogger: logger.mylogger
}
