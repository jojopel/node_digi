exports.date = function (next) {
  next(new Date().toLocaleString('en-US', {timeZone: 'Europe/Athens'}))
}
