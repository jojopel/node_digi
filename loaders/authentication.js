const passport = require('passport')
// for session
// const session = require('express-session')
// const MongoStore = require('connect-mongo')(session)

const cookieParser = require('cookie-parser')
const { Strategy } = require('passport-local')
// for jwt
const JWTStrategy = require('passport-jwt').Strategy
// const { ExtractJwt } = require('passport-jwt')
const db = require('../models')
const config = require('../config')
const env = process.env.NODE_ENV ? process.env.NODE_ENV : 'development'

module.exports = async (server) => {
  server.use(cookieParser())

  // FOR SESSION AUTH ONLY
  // const mongoStore = new MongoStore({
  //   url: config[env].storeurl,
  //   secret: config[env].sessionSecret,
  //   collection: 'sessions',
  //   ttl: 24 * 60 * 60, // 24h time to live
  //   touchAfter: 24 * 60 * 60,
  //   mongoOptions: {
  //     useNewUrlParser: true,
  //     useUnifiedTopology: true
  //   }
  // })
  // mongoStore.on('create', (id) => console.log('created session', id))
  // mongoStore.on('destroy', (id) => console.log('destroyed session', id))

  // const sessOptions = {
  //   secret: config[env].sessionSecret,
  //   name: 'digi_sid',
  //   cookie: {
  //     maxAge: 24 * 60 * 60 * 1000, // 24h
  //     httpOnly: false
  //   },
  //   resave: false,
  //   saveUninitialized: false,
  //   unset: 'destroy',
  //   store: mongoStore
  // }
  // server.use(session(sessOptions))

  // if (process.env.NODE_ENV === 'production') {
  // // options.cookie.secure = true // Serve secure cookies, requires HTTPS
  // }

  // console.log(`*** Store connection established ***`)

  // server.use(passport.initialize())
  // server.use(passport.session())

  // passport.deserializeUser((id, done) => {
  //   db.users.findByPk(id)
  //     .then((user) => {
  //       if (user) {
  //         done(null, user)
  //       }
  //     })
  //     .catch((err) => {
  //       done(err, null)
  //     })
  // })

  // passport.serializeUser((user, done) => {
  //   return done(null, user.user_id)
  // })

  passport.use(new Strategy(
    {
      usernameField: 'username',
      passwordField: 'password'
    },
    async (username, password, done) => {
      let passMatches
      let user
      try {
        user = await db.users.findByUsername(username)

        if (!user) {
          return done(null, false)
        }

        passMatches = await user.verifyPassword(password)

        if (!passMatches) {
          return done(null, false)
        }
      } catch (err) {
        return done(err)
      }
      return done(null, user)
    }
  ))

  const cookieExtractor = (req) => {
    let token = null
    if (req && req.cookies) {
      token = req.cookies['jwt']
    }
    return token
  }
  const JWTOptions = {
    jwtFromRequest: cookieExtractor,
    secretOrKey: config[env].jwtSecret
  }
  passport.use(new JWTStrategy(JWTOptions, async (tokenPayload, done) => {
    try {
      const {data: {id, ...rest}} = tokenPayload
      let user = await db.users.findByPk(id)
      if (!user) {
        return done(null, false)
      }
      return done(null, user)
    } catch (err) {
      return done(err)
    }
  })
  )
}

// User.prototype.generateJWT = function () {
//   const data = {
//     id: this.user_id,
//     firstname: this.firstname,
//     role: this.role_id
//   }
//   const signature = process.env.JWT_SECRET
//   const expiration = '24h'

//   return jwt.sign({ data }, signature, { expiresIn: expiration })
// }
