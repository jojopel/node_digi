// import compression from 'compression'
// import cors from 'cors'
// import helmet from 'helmet'
// import bodyParser from 'body-parser'
// import middlewares from '../middlewares'
const middlewares = require('../middlewares')
const cors = require('cors')
// const helmet = require('helmet')
const compression = require('compression')
const bodyParser = require('body-parser')
const engines = require('consolidate')
const path = require('path')
const serveStatic = require('serve-static')

module.exports = async (server) => {
  server.engine('html', engines.mustache)
  server.set('view engine', 'html')
  server.set('views', path.join(__dirname, '../../', 'projects_dist/'))
  server.use(compression())
  server.use(bodyParser.json(
    {
      limit: '50mb'
    }
  ))
  server.use(bodyParser.urlencoded(
    {
      extended: true,
      limit: '50mb'
    })
  )
  server.use(
    cors({
      // allowHeaders: ['Content-type', 'Authorization', 'Session', 'Origin'],
      // methods: ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
      // origin: /$/,
      credentials: true,
      origin: true
    })
  )
  // server.use(helmet())         problem sto iview load with x-frame-options not supported in many browsers
  // server.use(helmet.frameguard(
  //   {
  //     action: 'allow-from',
  //     domain: 'http://localhost:8080' },
  //   {
  //     action: 'allow-from',
  //     domain: 'http://10.10.10.115'
  //   }
  // ))
  server.use(serveStatic(path.join(__dirname, '../../', 'projects_dist/')))
  server.use(middlewares.logToConsole)
}
