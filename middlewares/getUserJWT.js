const db = require('../models')
module.exports = async (req, res, next) => {
  const decodedTokenData = req.jwt_token
  const user = await db.users.findByPk(decodedTokenData.data.id)
  if (!user) {
    return res.status(401).json({
      error: false,
      message: 'User not found'
    })
  }
  req.currentUserJWT = user
  return next()
}
