// import logToConsole from './apiLogger'
// import isLoggedIn from './isLoggedIn'
// import notFound from './notFound'
// // import { devHandler, prodHandler } from './errorHandler'
// import getUserJWT from './getUserJWT'
// import isAuthJWT from './isAuthJWT'

const logToConsole = require('./apiLogger')
const notFound = require('./notFound')
const getUserJWT = require('./getUserJWT')
const isAuthenticated = require('./isAuthenticated')
const attachPackage = require('./attachPackage')

const isEditor = require('./isEditor')
const isAdmin = require('./isAdmin')

const middlewares = {
  logToConsole,
  isAuthenticated,
  notFound,
  // devHandler,
  // prodHandler,
  getUserJWT,
  isEditor,
  isAdmin,
  attachPackage
}
// export default middlewares
module.exports = middlewares
