module.exports = function logToConsole (req, res, next) {
  console.log(`${req.method.toUpperCase()} request was made to: ${req.originalUrl}`)
  next()
}
