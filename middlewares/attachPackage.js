const db = require('../models')
module.exports = async (req, res, next) => {
  try {
    let pckg = await db.packages.findOne({
      where: {package_name: req.params.package_name}
    })
    if (pckg) {
      req.package = pckg
      console.log('middal', req.package.id)
      next()
    }
  } catch (err) {
    return res.status(403).json({
      error: true,
      message: 'No package found'
    })
  }
}
