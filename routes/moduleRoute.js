const moduleRouter = require('express').Router()
const moduleController = require('../controllers/moduleController')
const middlewares = require('../middlewares')

moduleRouter.use(middlewares.isAuthenticated)

// end point to create new module
moduleRouter.post('/create', moduleController.createModule)
// end point to update module with given id
moduleRouter.post('/update', moduleController.updateModule)
// end point to return all modules for specific package id
moduleRouter.post('/modules', moduleController.getPackageModules)
// end point to return all modules for specific package id,????
moduleRouter.get('/find/:id', moduleController.getPackageModules2)
// end point create and update multiple modules
moduleRouter.post('/insert', moduleController.insertModule)
// end point to retrieve all module
moduleRouter.post('/allmodules', moduleController.allModules)
// end point to count all modules
moduleRouter.post('/count', moduleController.countModules)
// end point to delete module with req.body.id
moduleRouter.post('/delete', moduleController.deleteModule)
// Each User Most Recent Modules based on package assign
moduleRouter.post('/myrecentmodules', moduleController.myRecentModules)
// create new module with all pages from requested modules
moduleRouter.post('/copymodule', moduleController.copyModule)

module.exports = moduleRouter
