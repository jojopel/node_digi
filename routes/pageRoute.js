const pageRouter = require('express').Router()
const pageController = require('../controllers/pageController')
const middlewares = require('../middlewares')

pageRouter.use(middlewares.isAuthenticated)

pageRouter.post('/recent', pageController.getRecent)
pageRouter.post('/create', pageController.createPage)
// end point to update page based on package_id
pageRouter.post('/update', pageController.updatePage)
// end point to update page module id
pageRouter.post('/updatemodule', pageController.updateModule)
// end point to update page json from builder
pageRouter.post('/updatejson', pageController.updateJson)
// end point to find page based on page_id
pageRouter.get('/findpage/:id', pageController.findPageById)
// end point to assign pages to digi activities
pageRouter.post('/assignpage', pageController.assignPage)
// end point to find page based on page_id
pageRouter.post('/nextpage', pageController.nextPage)
// end point to return all pages for specific module id
pageRouter.post('/pages', pageController.getModulePages)
// end point to unlock page with given id
pageRouter.post('/unlockpage', pageController.unlockPage)
// end point to lock page with given id
pageRouter.post('/lockpage', pageController.lockPage)
// end point create and update multiple pages
pageRouter.post('/insert', pageController.insertPage)
// delete page
pageRouter.post('/delete', pageController.deletePage)
// end point to count all pages
pageRouter.post('/count', pageController.countPages)
// Each User Most Recent Modules based on package assign
pageRouter.post('/myrecentpages', pageController.myRecentPages)

module.exports = pageRouter
