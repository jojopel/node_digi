
const bugRouter = require('express').Router()
const bugController = require('../controllers/bugController')
const middlewares = require('../middlewares')

bugRouter.use(middlewares.isAuthenticated)

bugRouter.get('/', bugController.getBugs)
bugRouter.post('/', bugController.addBug)
bugRouter.get('/:id', bugController.getBug)
bugRouter.put('/:id', bugController.updateBug)
bugRouter.put('/status/:id', middlewares.isAdmin, bugController.changeBugStatus)
bugRouter.delete('/:id', middlewares.isAdmin, bugController.deleteBug)
bugRouter.post('/paginateCount', bugController.paginateAndCount)
bugRouter.post('/paginate', bugController.paginate)
bugRouter.post('/searchCount', bugController.searchPaginateAndCount)
bugRouter.post('/search', bugController.searchPaginate)

module.exports = bugRouter
