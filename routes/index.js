const bugRouter = require('./bugRoute')
const userRouter = require('./userRoute')
const authRouter = require('./authRoute')
const packageRouter = require('./packageRoute')
const pageRouter = require('./pageRoute')
const moduleRouter = require('./moduleRoute')
const sectionRouter = require('./sectionRoute')
const pageHistoryRouter = require('./pageHistoryRoute')
const viewPackageRouter = require('./viewPackageRoute')
const middlewares = require('../middlewares')

const init = (server) => {
  server.get('/', (req, res) => {
    res.send('Home page')
  })
  server.get('/about', (req, res) => {
    res.send('Learn about us')
  })

  // server.use('/user', require('../controllers/user'))
  // server.use('/package', require('../controllers/package'))
  // server.use('/module', require('../controllers/module'))
  // server.use('/page', require('../controllers/page'))
  // server.use('/section', require('../controllers/section'))
  // server.use('/pagehistory', require('../controllers/pagehistory'))
  // server.use('/viewpackage', require('../controllers/viewpackage'))

  server.use('/mediaupload', middlewares.logToConsole, require('../controllers/mediaupload'))
  server.use('/pilotversions', require('../controllers/pilotversions'))
  server.use('/exportmodule', require('../controllers/export_module'))

  server.use('/bugs', bugRouter)
  server.use('/users', userRouter)
  server.use('/auth', authRouter)
  server.use('/packages', packageRouter)
  server.use('/pages', pageRouter)
  server.use('/module', moduleRouter)
  server.use('/section', sectionRouter)
  server.use('/pagehistory', pageHistoryRouter)
  server.use('/viewpackage', viewPackageRouter)

  server.use('*', middlewares.notFound)
}
module.exports = init
