const middlewares = require('../middlewares')
const authRouter = require('express').Router()
const controllerContainer = require('../controllers/controllerContainer')

authRouter.post('/signin', controllerContainer.authenticationController.signIn)

authRouter.post('/logout', middlewares.isAuthenticated, controllerContainer.authenticationController.signOut)

authRouter.get('/me', controllerContainer.authenticationController.getCurrentUser)

module.exports = authRouter
