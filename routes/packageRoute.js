const packageRouter = require('express').Router()
const packageController = require('../controllers/packageController')
const middlewares = require('../middlewares')

packageRouter.use(middlewares.isAuthenticated)

packageRouter.get('/recent', packageController.getRecent)
packageRouter.get('/', packageController.getPackages)
// end point to return single package info
packageRouter.get('/package/:package_name', middlewares.attachPackage, packageController.getPackage)
// end point to return all un-assigned users
packageRouter.get('/package/:package_name/unassigned', packageController.getPackageUnassignedUsers)
// Assign user to project
packageRouter.post('/package/:package_name/assign', packageController.assignUserToPackage)
// Unassign user to project
packageRouter.post('/package/:package_name/unassign', packageController.unAssignUserFromPackage)
// end point to return single package modules
packageRouter.post('/package/:package_name/modules', packageController.getPackageModules)
// end point to return single package modules count
packageRouter.get('/package/:package_name/modules/count', packageController.getPackageModulesCount)

// CLEAR ASSETS
packageRouter.post('/clearassets', packageController.clearAssets)
// end point to create new packages
packageRouter.post('/create', middlewares.isAdmin, packageController.createPackage)
// end point to gridview all packages
packageRouter.post('/gridview', packageController.gridView)
// end point to assigned users to packages
packageRouter.post('/assigned', middlewares.isAdmin, packageController.getAssigned)
// end point to delete packagestousers associations
packageRouter.get('/deletepackagesusers/:id', middlewares.isAdmin, packageController.deletePackageUsers)
// end point to update package with given id
packageRouter.post('/update', middlewares.isAdmin, packageController.updatePackage)
// end point to return package with given id
packageRouter.get('/find/:id', packageController.findPackage)
// end point to all packages  returned as array with id and package name
packageRouter.post('/packages', packageController.findAllPackages)
// end point to delete package
packageRouter.post('/delete', middlewares.isAdmin, packageController.deletePackage)
// end point to all categories  returned as array with id and category name
packageRouter.post('/categories', packageController.getPackageCategories)
// end point to all sections  returned as array with id and section name
packageRouter.post('/sections', middlewares.isAdmin, packageController.getSections)
// return all available packages
packageRouter.get('/allpackages', middlewares.isAdmin, packageController.getAllPackages)

module.exports = packageRouter
