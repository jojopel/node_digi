const viewPackageRouter = require('express').Router()
const viewPackageController = require('../controllers/viewPackageController')
const middlewares = require('../middlewares')

viewPackageRouter.use(middlewares.isAuthenticated)

viewPackageRouter.get('/packageiframe/:id', viewPackageController.viewPackageById)

viewPackageRouter.get('/packageiframe/static/css/:file', viewPackageController.getCssFile)

viewPackageRouter.get('/packageiframe/static/js/:file', viewPackageController.getJsFile)

viewPackageRouter.get('/packageiframe/static/img/:file', viewPackageController.getImgFile)

viewPackageRouter.get('/packageiframe/static/media/:file', viewPackageController.getMediaFile)

module.exports = viewPackageRouter
