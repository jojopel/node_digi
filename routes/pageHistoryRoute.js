const pageHistoryRouter = require('express').Router()
const pageHistoryController = require('../controllers/pageHistoryController')
const middlewares = require('../middlewares')

pageHistoryRouter.use(middlewares.isAuthenticated)

// end point to create pagehistory
pageHistoryRouter.post('/create', pageHistoryController.createPageHistory)
// get all page history
pageHistoryRouter.get('/pagehistory/:id', pageHistoryController.getPageHistory)
// delete page history
pageHistoryRouter.get('/deletepagehistory/:id', pageHistoryController.deletePageHistory)

module.exports = pageHistoryRouter
