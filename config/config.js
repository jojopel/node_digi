var development = require('./default.json')
var production = require('./default.json')

module.exports = {
  development: {
    username: development.dbuser,
    password: development.dbpwd,
    database: development.dbname,
    host: development.host,
    dialect: 'mysql',
    operatorsAliases: false
  },
  test: {
    username: process.env.DBNAME,
    password: process.env.DBPWD,
    database: process.env.DBNAME,
    host: process.env.HOST,
    dialect: 'mysql',
    operatorsAliases: false
  },
  production: {
    username: production.dbuser,
    password: production.dbpwd,
    database: production.dbname,
    host: production.host,
    dialect: 'mysql',
    operatorsAliases: false
  }
}
