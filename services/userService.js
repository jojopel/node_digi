const db = require('../models')

const userService = {
  async getUser (userId) {
    try {
      let user = await db.users.findById(userId)
      return user.toMediumFields()
    } catch (error) {
      console.log(error)
    }
  },
  async assignUserToPackage (userId, projectId) {
    try {
      let user = await db.users.findById(userId)
      let projectToAssign = await db.packages.findById(projectId)
      return user.addProject(projectToAssign)
    } catch (error) {
      console.log(error)
    }
  },
  async removeUserFromPackage (userId) {
    try {
      let user = await db.users.findById(userId)
      return user.toMediumFields()
    } catch (error) {
      console.log(error)
    }
  }

}
module.exports = userService
