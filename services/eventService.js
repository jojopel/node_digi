const Emittery = require('emittery')
const listenerFunctionContainer = require('../listeners')
const eventService = new Emittery()

eventService.on('bugCreatedEmail', listenerFunctionContainer.bugListenerFunctions.notifyAdminBugEmail)
eventService.on('bugEventSlack', listenerFunctionContainer.bugListenerFunctions.notifyAdminBugSlack)

module.exports = eventService
