const axios = require('axios')

const slackWebhookUrl = 'https://hooks.slack.com/services/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'
const slackService = {
  async generateBugMessage (bug) {
    let issuer
    let packageName
    let moduleName
    let pageName
    let text = 'A bug was updated at the following fields: '
    try {
      issuer = await bug.getIssuerUsername()
      packageName = await bug.getPackageName()
      moduleName = await bug.getModuleName()
      pageName = await bug.getPageName()
    } catch (e) {
      throw new Error('Failed to get field', e)
    }
    if (bug._options.isNewRecord) {
      text = 'New bug submitted!'
    }
    if (bug.changed('bug_title')) {
      text += ' TITLE '
    }
    if (bug.changed('bug_description')) {
      text += ' DESCRIPTION '
    }
    // if (bug.changed('completed') && !bug.completed) {
    //   text += ' STATUS '
    // }
    if (bug.changed('completed') && bug.completed) {
      text = 'A bug was exterminated!'
    }
    if (bug.changed('reopen') && bug.reopen) {
      text = 'A bug was reopened!'
    }

    let message = {
      'username': 'Bug bot',
      'text': text,
      'mrkdwn': true,
      'icon_emoji': ':bug:',
      'attachments': [{
        'fallback': `*Bug details* : <http://${process.env.DOMAIN}/#/bugs/${bug.bug_id}| _${bug.bug_id}_>`,
        'pretext': `*Bug details* : <http://${process.env.DOMAIN}/#/bugs/${bug.bug_id}| _${bug.bug_id}_>`,
        'color': 'warning',
        'fields': [
          {
            'title': 'Issuer',
            'value': issuer,
            'short': true
          },
          {
            'title': 'Package',
            'value': packageName,
            'short': true
          },
          {
            'title': 'Module',
            'value': moduleName,
            'short': true
          },
          {
            'title': 'Page',
            'value': pageName,
            'short': true
          }, {
            'title': 'Title',
            'value': bug.bug_title,
            'short': true
          },
          {
            'title': 'Description',
            'value': bug.bug_description,
            'short': false
          }
        ],
        'actions': [
          {
            'type': 'button',
            'text': `Exterminate :hammer:!`,
            'style': 'primary',
            'url': `http://localhost:8080/#/bugs/${bug.bug_id}`
          }
        ]
      }]
    }
    try {
      message = JSON.stringify(message)
    } catch (e) {
      throw new Error('Failed to stringify message', e)
    }
    return Promise.resolve(message)
  },

  async sendSlackMessage  (message) {
    try {
      let resp = await axios.post(slackWebhookUrl, message)
      return Promise.resolve(resp)
    } catch (e) {
      throw new Error('Failed to send Slack message', e)
    }
  }
}
module.exports = slackService
